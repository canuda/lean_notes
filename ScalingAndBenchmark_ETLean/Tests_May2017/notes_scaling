
ET-Lean is based on the Cactus Computational Toolkit, using the Carpet package for the block-structured AMR infrastructure. Each AMR box is its own independent domain, which is  distributed to different processing nodes via MPI. This is a crucial ingredient to resolve the very different length scales arising for our typical evolutions.

Our scaling tests were based on a set of simulations evolving the early stages of a head-on black hole collision from rest. Such a configuration should be fairly representative of the typical runs we will perform in the context of the present proposal and it has the further advantage of the initial data generation being trivial. The tests were performed on the Cosmos cluster, consisting of ... [Helvi, please complete].

For tests reported here, we used a grid setup of the type

 { (120, 32, 16, 8, 4) x (1.6, 0.6), h = hf }

that is, there are five fixed outer grids of radius 120, 32, 16, 8 and 4, respectively, and two (moving) refinement components each with radius 1.6 and 0.6 centred around either hole.


We now report on our strong scaling test, where we fix the grid setup above and varied the number of cores for a fixed problem size (resolution of hf = 160). Here we evolved our configuration for 1 hour of physical time. We show the results in the following table and in figure [Lean_strong_scaling.pdf]

#cores #steps #speedup
 32     541   1.00
 64     981   1.81
128    1812   3.34
256    2959   5.46
512    5042   9.31
1024   3419   6.32



We observe a decrease below the expected speedup when using more than 200 cores. We attribute
this decrease in the scaling performance to the significant increasing overhead introduced in the inter-processor communication, exacerbated by the fact that the resolution used was quite modest for the number of processors requested. To verify whether this interpretation is correct, we have also performed a weak scaling test.


For our weak scaling test we fix the grid setup above and vary the resolution hf and number of cores in such a way as to maintain the number of points per core approximately constant. For this test we fixed the total number of timesteps evolved to be 1000 and, keeping the number of points per core approximately constant, we compared the total execution time. Results can be seen in the following table and in figure [Lean_weak_scaling.pdf]

#cores  #time (secs)
   8    1856
  24    1561
  48    1451
  96    1446
 192    1466
 384    1344
1024    1958


As expected for a weak scaling test, the execution time remains approximately constant as we increase the number of processors, showing that the code does weakly scale as expected.
