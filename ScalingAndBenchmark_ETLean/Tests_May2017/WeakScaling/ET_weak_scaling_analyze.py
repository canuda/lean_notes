#!/usr/bin/env python

from __future__ import division
from __future__ import print_function

import numpy as np

import matplotlib
import matplotlib.pyplot as plt 


def RE(ntotcells, nsteps, nsec, ncores):
    return 1.0 * ntotcells * nsteps / (nsec * ncores)


ncells_list = [
    1367631 + 181500 + 196625 + 229900 + 290400 + 308550 + 179860,
    3796416 + 426320 + 468952 + 548887 + 708757 + 740731 + 427915,
    7301384 + 751168 + 828608 + 968000 + 1262272 + 1308736 + 740731,
    14886936 + 1437601 + 1580173 + 1865317 + 2435605 + 2506891 + 1401300,
    28652616 + 2617972 + 2883307 + 3413977 + 4475317 + 4581451 + 2506891,
    55306341 + 4835558 + 5313800 + 6323422 + 8316097 + 8475511 + 4581451,
]

nsteps_list = [
    1637,
    1948,
    1919,
    1975,
    2015,
    2101,
]

nsec_list = [
    3600,
    3600,
    3600,
    3600,
    3600,
    3600,
]

ncores_list = [
    8,
    24,
    48,
    96,
    192,
    384,
]

nruns = len(ncores_list)

eff_list   = np.zeros(nruns)


for i in range(nruns):

    ncells     = ncells_list[i]
    nsteps     = nsteps_list[i]
    nsec       = nsec_list[i]
    ncores     = ncores_list[i]

    # runtime efficiency
    eff = RE(ncells, nsteps, nsec, ncores)

    eff_list[i]  = eff



fig=plt.figure(1)
fig.clf()

ax  = fig.add_subplot(111)

ax.set_title('Weak Scaling')
ax.set_xlabel('#cpu')
ax.set_ylabel('Efficiency')

# ax.set_xlim([0,120])
# ax.set_ylim([-2.1e-6,2.1e-6])

ax.plot(ncores_list, eff_list, 'x-')

dirout  = './'
fileout = 'ET_weak_scaling'
fig.savefig(dirout + '/' + fileout + '.pdf')

# np.savetxt(dirout + '/' + 'weak.dat', np.array([ncores_list, eff_list]).T )

plt.show()
