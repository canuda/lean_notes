#!/usr/bin/env python

from __future__ import division
from __future__ import print_function

import numpy as np

import matplotlib
import matplotlib.pyplot as plt 


def RE(ntotcells, nsteps, nsec, ncores):
    return 1.0 * ntotcells * nsteps / (nsec * ncores)


#TODO:
ncells_list = np.array([
    1367631 + 181500 + 196625 + 229900 + 290400 + 308550 + 179860,
    3796416 + 426320 + 468952 + 548887 + 708757 + 740731 + 427915,
    7301384 + 751168 + 828608 + 968000 + 1262272 + 1308736 + 740731,
    14886936 + 1437601 + 1580173 + 1865317 + 2435605 + 2506891 + 1401300,
    28652616 + 2617972 + 2883307 + 3413977 + 4475317 + 4581451 + 2506891,
    55306341 + 4835558 + 5313800 + 6323422 + 8316097 + 8475511 + 4581451,
    147197952 + 12222432 + 13503816 + 16066584 + 21192120 + 21487824 + 11437200,
])

#TODO:
nsteps_list = np.array([
    1939,
    2305,
    2481,
    2489,
    2455,
    2677,
    1838,    
])

#TODO:
nsec_list = np.array([
    3600,
    3600,
    3600,
    3600,
    3600,
    3600,
    3600,
])

#TODO:
ncores_list = np.array([
    8,
    24,
    48,
    96,
    192,
    384,
    1024,
])

#TODO:
#vel

#TODO:
nruns = len(ncores_list)

#TODO:
eff_list   = np.zeros(nruns)


for i in range(nruns):

#TODO:
    ncells     = ncells_list[i]
    nsteps     = nsteps_list[i]
    nsec       = nsec_list[i]
    ncores     = ncores_list[i]

#TODO:
    # runtime efficiency
    eff = RE(ncells, nsteps, nsec, ncores)

#TODO:
    eff_list[i]  = eff


#TODO:
secs_per_1000steps = ncells_list * 1000 / (ncores_list * eff_list)



fig=plt.figure(1)
fig.clf()

ax  = fig.add_subplot(111)

ax.set_title('Weak Scaling Lean')
ax.set_xlabel('#cpu')
ax.set_ylabel('Time (secs)')

# ax.set_xlim([0,120])
# ax.set_ylim([-2.1e-6,2.1e-6])

# ax.plot(ncores_list, eff_list, 'x-')
ax.plot(ncores_list, secs_per_1000steps, 'x-')

dirout  = './'
fileout = 'Lean_weak_scaling'
fig.savefig(dirout + '/' + fileout + '.pdf')

np.savetxt(dirout + '/' + 'weak.dat', np.array([ncores_list,secs_per_1000steps]).T )

plt.show()
