# Scalar ASCII output created by CarpetIOScalar
# created on cosmos2.private.damtp.cam.ac.uk by hw401 on May 21 2017 at 13:46:31+0100
# parameter filename: "Lean_head-on_d06_hf128_weak_np96.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 575 0 519 0 0 24 487 31 9 0
256 1 582 0 519 0 0 24 487 31 17 0
512 2 582 0 519 0 0 24 487 31 17 0
768 3 582 0 519 0 0 24 487 31 17 0
1024 4 583 0 519 0 0 24 487 31 17 0
1280 5 583 0 519 0 0 24 487 31 17 0
1536 6 583 0 519 0 0 24 487 31 17 0
1792 7 583 0 519 0 0 24 487 31 17 0
2048 8 583 0 519 0 0 24 487 31 17 0
2304 9 583 0 519 0 0 24 487 31 17 0
