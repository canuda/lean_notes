# Scalar ASCII output created by CarpetIOScalar
# created on cosmos2.private.damtp.cam.ac.uk by hw401 on May 26 2017 at 17:46:03+0100
# parameter filename: "Lean_head-on_d06_hf278_weak_np1024.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 909 0 501 0 0 256 468 27 20 0
256 0.46 923 0 501 0 0 256 468 27 21 0
512 0.92 925 0 501 0 0 256 468 27 21 0
768 1.38 926 0 501 0 0 256 468 27 21 0
1024 1.84 927 0 501 0 0 256 468 27 21 0
1280 2.3 928 0 501 0 0 256 468 27 21 0
1536 2.76 928 0 501 0 0 256 468 27 21 0
1792 3.22 929 0 501 0 0 256 468 27 21 0
