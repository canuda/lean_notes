# Scalar ASCII output created by CarpetIOScalar
# created on cosmos2.private.damtp.cam.ac.uk by hw401 on May 21 2017 at 22:40:17+0100
# parameter filename: "Lean_head-on_d06_hf200_weak_np384.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 657 0 489 0 0 96 456 29 0 0
256 0.64 673 0 489 0 0 96 456 29 10 0
512 1.28 675 0 489 0 0 96 456 29 10 0
768 1.92 675 0 489 0 0 96 456 29 10 0
1024 2.56 675 0 489 0 0 96 456 29 10 0
1280 3.2 676 0 489 0 0 96 456 29 10 0
1536 3.84 676 0 489 0 0 96 456 29 10 0
1792 4.48 676 0 489 0 0 96 456 29 10 0
2048 5.12 676 0 489 0 0 96 456 29 10 0
2304 5.76 676 0 489 0 0 96 456 29 10 0
2560 6.4 676 0 489 0 0 96 456 29 10 0
