# Scalar ASCII output created by CarpetIOScalar
# created on cosmic.private.damtp.cam.ac.uk by hw401 on May 22 2017 at 18:16:00+0100
# parameter filename: "Lean_head-on_d06_hf55_weak_np8.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 630 0 635 0 0 2 597 35 16 0
256 2.3 630 0 635 0 0 2 597 35 16 0
512 4.6 630 0 635 0 0 2 597 35 16 0
768 6.9 630 0 635 0 0 2 597 35 16 0
1024 9.2 630 0 635 0 0 2 597 35 16 0
1280 11.5 630 0 635 0 0 2 597 35 16 0
1536 13.8 630 0 635 0 0 2 597 35 16 0
1792 16.1 630 0 635 0 0 2 597 35 16 0
