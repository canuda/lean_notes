# Scalar ASCII output created by CarpetIOScalar
# created on cosmos2.private.damtp.cam.ac.uk by hw401 on May 21 2017 at 12:51:41+0100
# parameter filename: "ML_head-on_d06_hf100_weak_np48.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 703 0 670 0 0 12 632 36 23 0
256 1.28 707 0 670 0 0 12 633 36 23 0
512 2.56 707 0 670 0 0 12 633 36 23 0
768 3.84 708 0 670 0 0 12 633 36 23 0
1024 5.12 708 0 670 0 0 12 633 36 23 0
1280 6.4 708 0 670 0 0 12 633 36 23 0
1536 7.68 708 0 670 0 0 12 633 36 23 0
1792 8.96 708 0 670 0 0 12 633 36 23 0
