# Scalar ASCII output created by CarpetIOScalar
# created on cosmos2.private.damtp.cam.ac.uk by hw401 on May 20 2017 at 22:14:26+0100
# parameter filename: "ML_head-on_d06_hf160_weak_np192.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 936 0 795 0 0 48 748 50 40 0
256 0.8 1013 0 795 0 0 48 748 50 40 0
512 1.6 1022 0 795 0 0 48 748 50 40 0
768 2.4 1026 0 795 0 0 48 748 50 40 0
1024 3.2 1028 0 795 0 0 48 748 50 40 0
1280 4 1029 0 795 0 0 48 748 50 40 0
1536 4.8 1029 0 795 0 0 48 748 50 40 0
1792 5.6 1030 0 795 0 0 48 748 50 40 0
