#!/bin/bash

### ============ Setup ==============================

#PBS -q super2
#PBS -V 
#PBS -j oe 
#PBS -N ML_head-on_d06_hf256_weak_np768
#PBS -l nodes=96:ppn=8
#PBS -l walltime=01:00:00

### ================================================

### ========= shell commands =======================
date

### ================================================

### ========= Execution ============================

CACTUS=/home/cosmos/users/hw401/Code/ET_Lean/Cactus/exe/cactus_Lean_WBSSN
RUNNAME=ML_head-on_d06_hf256_weak_np768
RUNPATH=/fast/space/projects/lean2013/hw401/ScalingAndBenchmark_ETLean/WeakScaling/MLZ4


cd $RUNPATH
NP=$PBS_NP

mpiexec_mpt -np $NP dplace -s1 $CACTUS $RUNNAME.par >& $RUNNAME.log

echo Done.

### ================================================
