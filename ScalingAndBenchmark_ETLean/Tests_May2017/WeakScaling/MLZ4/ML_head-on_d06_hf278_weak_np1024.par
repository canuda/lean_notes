
# to be run with #cores = 768

#------------------------------------------------------------------------------
ActiveThorns = "
  ADMBase
  ADMCoupling
  ADMMacros
  AEILocalInterp
  # AHFinderDirect
  Boundary
  Carpet
  CarpetInterp
  CarpetIOASCII
  CarpetIOBasic
  CarpetIOHDF5
  CarpetIOScalar
  CarpetLib
  CarpetMask
  CarpetReduce
  CarpetRegrid2
  CarpetSlab
  CarpetTracker
  CartGrid3D
  CoordBase
  CoordGauge
  Dissipation
  # Formaline
  Fortran
  GenericFD
  GSL
  HDF5
  InitBase
  IOUtil
  LocalInterp
  LoopControl
  ML_ADMConstraints
  ML_BSSN
  ML_BSSN_Helper
  MoL
  # Multipole
  # NaNChecker
  NewRad
  # PunctureTracker
  # QuasiLocalMeasures
  ReflectionSymmetry
  # RotatingSymmetry180
  Slab
  SpaceMask
  SphericalSurface
  StaticConformal
  SummationByParts
  SymBase
  SystemStatistics
  TerminationTrigger
  Time
  TimerReport
  TmunuBase
  TwoPunctures
  Vectors
  # WeylScal4
"
#------------------------------------------------------------------------------


# Grid setup
#------------------------------------------------------------------------------

CartGrid3D::type                     = "coordbase"
Carpet::domain_from_coordbase        = yes
CoordBase::domainsize                = "minmax"

# make sure all (xmax - xmin)/dx are integers!
CoordBase::xmin                       =    0.00
CoordBase::ymin                       =    0.00
CoordBase::zmin                       =    0.00
CoordBase::xmax                       = +120.06
CoordBase::ymax                       = +120.06
CoordBase::zmax                       = +120.06
CoordBase::dx                         =    0.23
CoordBase::dy                         =    0.23
CoordBase::dz                         =    0.23

driver::ghost_size                    = 3

CoordBase::boundary_size_x_lower      = 3
CoordBase::boundary_size_y_lower      = 3
CoordBase::boundary_size_z_lower      = 3
CoordBase::boundary_size_x_upper      = 3
CoordBase::boundary_size_y_upper      = 3
CoordBase::boundary_size_z_upper      = 3

CoordBase::boundary_shiftout_x_lower  = 1
CoordBase::boundary_shiftout_y_lower  = 1
CoordBase::boundary_shiftout_z_lower  = 1

CarpetRegrid2::symmetry_rotating180   = no

ReflectionSymmetry::reflection_x      = yes
ReflectionSymmetry::reflection_y      = yes
ReflectionSymmetry::reflection_z      = yes
ReflectionSymmetry::avoid_origin_x    = no
ReflectionSymmetry::avoid_origin_y    = no
ReflectionSymmetry::avoid_origin_z    = no


# Mesh refinement
#------------------------------------------------------------------------------

Carpet::max_refinement_levels           = 7

CarpetRegrid2::num_centres              = 2

CarpetRegrid2::num_levels_1             = 7
CarpetRegrid2::position_x_1             = 3
CarpetRegrid2::radius_1[1]              = 24.0
CarpetRegrid2::radius_1[2]              = 12.0
CarpetRegrid2::radius_1[3]              = 6.0
CarpetRegrid2::radius_1[4]              = 3.0
CarpetRegrid2::radius_1[5]              = 1.5
CarpetRegrid2::radius_1[6]              = 0.6
CarpetRegrid2::movement_threshold_1     = 0.16

CarpetRegrid2::num_levels_2             = 7
CarpetRegrid2::position_x_2             = -3
CarpetRegrid2::radius_2[1]              = 24.0
CarpetRegrid2::radius_2[2]              = 12.0
CarpetRegrid2::radius_2[3]              = 6.0
CarpetRegrid2::radius_2[4]              = 3.0
CarpetRegrid2::radius_2[5]              = 1.5
CarpetRegrid2::radius_2[6]              = 0.6
CarpetRegrid2::movement_threshold_2     = 0.16

Carpet::use_buffer_zones                = yes
Carpet::prolongation_order_space        = 5
Carpet::prolongation_order_time         = 2

CarpetRegrid2::freeze_unaligned_levels  = yes
CarpetRegrid2::regrid_every             = 64

CarpetRegrid2::verbose                  = no

Carpet::grid_structure_filename         = "carpet-grid-structure"
Carpet::grid_coordinates_filename       = "carpet-grid-coordinates"

Carpet::time_refinement_factors         = "[1, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512]"

Time::dtfac                             = 0.25


# Initial Data
#------------------------------------------------------------------------------

ADMBase::initial_data                 = "twopunctures"
ADMBase::initial_lapse                = "psi^n"
ADMBase::initial_shift                = "zero"
ADMBase::initial_dtlapse              = "zero"
ADMBase::initial_dtshift              = "zero"

ADMBase::lapse_timelevels             = 3
ADMBase::shift_timelevels             = 3
ADMBase::metric_timelevels            = 3

# TwoPunctures::target_M_plus           = 0.5
# TwoPunctures::target_M_minus          = 0.5

TwoPunctures::par_m_plus              = 0.5
TwoPunctures::par_m_minus             = 0.5

TwoPunctures::par_b                   = 3
TwoPunctures::center_offset[0]        = 0

TwoPunctures::par_P_plus[0]           = 0.
TwoPunctures::par_P_plus[1]           = 0.
TwoPunctures::par_P_plus[2]           = 0.

TwoPunctures::par_P_minus[0]          = 0.
TwoPunctures::par_P_minus[1]          = 0.
TwoPunctures::par_P_minus[2]          = 0.

TwoPunctures::par_S_plus[0]           = 0.
TwoPunctures::par_S_plus[1]           = 0.
TwoPunctures::par_S_plus[2]           = 0.

TwoPunctures::par_S_minus[0]          = 0.
TwoPunctures::par_S_minus[1]          = 0.
TwoPunctures::par_S_minus[2]          = 0.

# TwoPunctures::give_bare_mass          = yes

TwoPunctures::TP_epsilon              = 1.0e-6
TwoPunctures::TP_Tiny                 = 1.e-10

TwoPunctures::verbose                 = yes

InitBase::initial_data_setup_method   = "init_all_levels"
Carpet::init_fill_timelevels          = no
Carpet::init_3_timelevels             = yes


# Evolution
#------------------------------------------------------------------------------

ADMBase::evolution_method         = "ML_BSSN"
ADMBase::lapse_evolution_method   = "ML_BSSN"
ADMBase::shift_evolution_method   = "ML_BSSN"
ADMBase::dtlapse_evolution_method = "ML_BSSN"
ADMBase::dtshift_evolution_method = "ML_BSSN"

ML_BSSN::harmonicN           = 1      # 1+log
ML_BSSN::harmonicF           = 2.0    # 1+log
ML_BSSN::ShiftGammaCoeff     = 0.75
ML_BSSN::BetaDriver          = 1.0
ML_BSSN::advectLapse         = 1
ML_BSSN::advectShift         = 1

ML_BSSN::MinimumLapse          = 1.0e-8
ML_BSSN::conformalMethod       = 1 # 1 for W


# Boundary conditions
#------------------------------------------------------------------------------

ML_BSSN::initial_boundary_condition = "extrapolate-gammas"
ML_BSSN::rhs_boundary_condition     = "NewRad"
Boundary::radpower                  = 2

ML_BSSN::ML_log_confac_bound = "none"
ML_BSSN::ML_metric_bound     = "none"
ML_BSSN::ML_Gamma_bound      = "none"
ML_BSSN::ML_trace_curv_bound = "none"
ML_BSSN::ML_curv_bound       = "none"
ML_BSSN::ML_lapse_bound      = "none"
ML_BSSN::ML_dtlapse_bound    = "none"
ML_BSSN::ML_shift_bound      = "none"
ML_BSSN::ML_dtshift_bound    = "none"


# Spatial finite differencing
#------------------------------------------------------------------------------

SummationByParts::order = 4
ML_BSSN::fdOrder        = 4

Dissipation::order = 5
Dissipation::vars  = "
  ML_BSSN::ML_log_confac
  ML_BSSN::ML_metric
  ML_BSSN::ML_trace_curv
  ML_BSSN::ML_curv
  ML_BSSN::ML_Gamma
  ML_BSSN::ML_lapse
  ML_BSSN::ML_shift
  ML_BSSN::ML_dtlapse
  ML_BSSN::ML_dtshift
"


# Integration method
#------------------------------------------------------------------------------

MoL::ODE_Method                 = "RK4"
MoL::MoL_Intermediate_Steps     = 4
MoL::MoL_Num_Scratch_Levels     = 1

Carpet::num_integrator_substeps = 4




# Timers
#-------------------------------------------------------------------------------

Cactus::cctk_timer_output               = "full"
TimerReport::out_every                  = 5120
TimerReport::n_top_timers               = 40
TimerReport::output_all_timers_together = yes
TimerReport::output_all_timers_readable = yes
TimerReport::output_schedule_timers     = no


# I/O thorns
#-------------------------------------------------------------------------------

Cactus::cctk_run_title       = $parfile
IO::out_dir                  = $parfile

IOScalar::one_file_per_group = yes
IOASCII::one_file_per_group  = yes

IOHDF5::use_checksums        = no
IOHDF5::one_file_per_group   = no

IOBasic::outInfo_every       = 256
IOBasic::outInfo_reductions  = "minimum maximum"
IOBasic::outInfo_vars        = "
  Carpet::physical_time_per_hour
  SystemStatistics::maxrss_mb
"

# for scalar reductions of 3D grid functions
IOScalar::outScalar_every               = 256
IOScalar::outScalar_reductions          = "minimum maximum average"
IOScalar::outScalar_vars                = "SystemStatistics::process_memory_mb"


# output just at one point (0D)
IOASCII::out0D_every = -1
IOASCII::out0D_vars  = "
"

IOASCII::output_symmetry_points = no
IOASCII::out3D_ghosts           = no

# 1D text output
IOASCII::out1D_every            = -1
IOASCII::out1D_d                = no
IOASCII::out1D_x                = yes
IOASCII::out1D_y                = no
IOASCII::out1D_z                = no
IOASCII::out1D_vars             = "
"

# 1D HDF5 output
#IOHDF5::out1D_every            = 256
#IOHDF5::out1D_d                = no
#IOHDF5::out1D_x                = yes
#IOHDF5::out1D_y                = no
#IOHDF5::out1D_z                = no
#IOHDF5::out1D_vars             = "
#  ADMBase::lapse
#"

# 2D HDF5 output
#IOHDF5::out2D_every             = 256
#IOHDF5::out2D_xy                = yes
#IOHDF5::out2D_xz                = no
#IOHDF5::out2D_yz                = no
#IOHDF5::out2D_vars              = "
#  ADMBase::lapse
#  ML_BSSN::ML_log_confac
#"

# # 3D HDF5 output
# IOHDF5::out_every                      = 8192
# IOHDF5::out_vars                       = "
#   ADMBase::lapse
# "

Carpet::verbose                    = no
Carpet::veryverbose                = no
Carpet::schedule_barriers          = no
Carpet::storage_verbose            = no
CarpetLib::output_bboxes           = no

Cactus::cctk_full_warnings         = yes
Cactus::highlight_warning_messages = no


# Checkpointing and recovery
#-------------------------------------------------------------------------------

CarpetIOHDF5::checkpoint             = no
IO::checkpoint_dir                   = "checkpoints_ML_head-on_d06_hf278_weak_np1024"
IO::checkpoint_ID                    = no
IO::checkpoint_every_walltime_hours  = 23
IO::checkpoint_on_terminate          = no
IO::out_proc_every                   = 2
IO::checkpoint_keep                  = 1

IO::recover                          = "autoprobe"
IO::recover_dir                      = "checkpoints_ML_head-on_d06_hf278_weak_np1024"

IO::abort_on_io_errors                      = yes
CarpetIOHDF5::open_one_input_file_at_a_time = yes
CarpetIOHDF5::compression_level             = 9


# Run termination
#-------------------------------------------------------------------------------

TerminationTrigger::max_walltime                 = 12 # hours
TerminationTrigger::on_remaining_walltime        = 30 # minutes
# TerminationTrigger::output_remtime_every_minutes = 30

Cactus::terminate       = "runtime"
Cactus::max_runtime     = 60.0
# Cactus::max_runtime     = 10.0
