# Scalar ASCII output created by CarpetIOScalar
# created on cosmos2.private.damtp.cam.ac.uk by hw401 on May 20 2017 at 21:57:19+0100
# parameter filename: "ML_head-on_d06_hf80_weak_np24.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 890 0 852 0 0 6 805 48 36 0
256 1.6 893 0 852 0 0 6 805 47 36 0
512 3.2 893 0 852 0 0 6 805 47 36 0
768 4.8 893 0 852 0 0 6 805 47 36 0
1024 6.4 893 0 852 0 0 6 805 47 36 0
1280 8 893 0 852 0 0 6 805 47 36 0
1536 9.6 893 0 852 0 0 6 805 47 36 0
1792 11.2 893 0 852 0 0 6 805 47 36 0
