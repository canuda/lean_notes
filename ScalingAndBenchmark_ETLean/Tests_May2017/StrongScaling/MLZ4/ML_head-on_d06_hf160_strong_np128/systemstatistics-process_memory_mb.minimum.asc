# Scalar ASCII output created by CarpetIOScalar
# created on cosmos2.private.damtp.cam.ac.uk by hw401 on May 20 2017 at 21:06:15+0100
# parameter filename: "ML_head-on_d06_hf160_strong_np128.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 991 0 917 0 0 32 859 53 10 0
256 0.8 1003 0 917 0 0 32 859 53 12 0
512 1.6 1003 0 917 0 0 32 859 53 12 0
768 2.4 1004 0 917 0 0 32 859 53 12 0
1024 3.2 1004 0 917 0 0 32 859 53 12 0
1280 4 1004 0 917 0 0 32 859 53 12 0
