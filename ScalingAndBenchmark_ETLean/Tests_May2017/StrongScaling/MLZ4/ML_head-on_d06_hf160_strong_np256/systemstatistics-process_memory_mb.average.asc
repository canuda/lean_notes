# Scalar ASCII output created by CarpetIOScalar
# created on cosmos2.private.damtp.cam.ac.uk by hw401 on May 20 2017 at 21:08:38+0100
# parameter filename: "ML_head-on_d06_hf160_strong_np256.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 733 0 586 0 0 64 553 31 22 0
256 0.8 770 0 586 0 0 64 554 31 24 0
512 1.6 773 0 586 0 0 64 554 31 24 0
768 2.4 775 0 586 0 0 64 554 31 24 0
1024 3.2 776 0 586 0 0 64 554 31 24 0
1280 4 777 0 586 0 0 64 554 31 24 0
1536 4.8 778 0 586 0 0 64 554 31 24 0
1792 5.6 778 0 586 0 0 64 554 31 24 0
2048 6.4 778 0 586 0 0 64 554 31 24 0
2304 7.2 779 0 586 0 0 64 554 31 24 0
