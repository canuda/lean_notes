#!/bin/bash

### ============ Setup ==============================

#PBS -q benchmark
#PBS -V 
#PBS -j oe 
#PBS -N ML_head-on_d06_hf160_strong_np1024
#PBS -l nodes=128:ppn=8
#PBS -l walltime=01:00:00

###PBS -q large2@cosmos2
###PBS -l mem=24GB

### ================================================

### ========= shell commands =======================
date

### ================================================

### ========= Execution ============================

CACTUS=/home/cosmos/users/hw401/Code/ET_Lean/Cactus/exe/cactus_Lean_WBSSN
RUNNAME=ML_head-on_d06_hf160_strong_np1024
RUNPATH=/fast/space/projects/lean2013/hw401/ScalingAndBenchmark_ETLean/StrongScaling/MLZ4


cd $RUNPATH
NP=$PBS_NP

mpiexec_mpt -np $NP dplace -s1 $CACTUS $RUNNAME.par >& $RUNNAME.log

echo Done.

### ================================================
