# Scalar ASCII output created by CarpetIOScalar
# created on cosmos2.private.damtp.cam.ac.uk by hw401 on May 27 2017 at 08:22:27+0100
# parameter filename: "ML_head-on_d06_hf160_strong_np1024.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 734 0 274 0 0 256 264 11 9 0
256 0.8 1039 0 274 0 0 256 264 73 72 0
512 1.6 1076 0 274 0 0 256 264 73 72 0
768 2.4 1093 0 274 0 0 256 264 73 72 0
1024 3.2 1116 0 274 0 0 256 264 73 72 0
1280 4 1141 0 274 0 0 256 264 73 72 0
1536 4.8 1157 0 274 0 0 256 264 73 72 0
1792 5.6 1171 0 274 0 0 256 264 73 72 0
2048 6.4 1181 0 274 0 0 256 264 73 72 0
2304 7.2 1190 0 274 0 0 256 264 73 72 0
2560 8 1197 0 274 0 0 256 264 73 72 0
2816 8.8 1202 0 274 0 0 256 264 73 72 0
