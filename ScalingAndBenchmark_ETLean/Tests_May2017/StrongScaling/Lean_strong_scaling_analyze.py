#!/usr/bin/env python

from __future__ import division
from __future__ import print_function

import numpy as np

import matplotlib
import matplotlib.pyplot as plt 


def RE(ntotcells, nsteps, nsec, ncores):
    return 1.0 * ntotcells * nsteps / (nsec * ncores)

ncells_list = np.array([
    28652616 + 2617972 + 2883307 + 3413977 + 4475317 + 4581451 + 2506891,
    28652616 + 2617972 + 2883307 + 3413977 + 4475317 + 4581451 + 2506891,
    28652616 + 2617972 + 2883307 + 3413977 + 4475317 + 4581451 + 2506891,
    28652616 + 2617972 + 2883307 + 3413977 + 4475317 + 4581451 + 2506891,
    28652616 + 2617972 + 2883307 + 3413977 + 4475317 + 4581451 + 2506891,
    28652616 + 2617972 + 2883307 + 3413977 + 4475317 + 4581451 + 2506891,
])

nsteps_list = np.array([
    541,
    981,
    1812,
    2959,
    5042,
    3419,
])

nsec_list = np.array([
    3600,
    3600,
    3600,
    3600,
    3600,
    3600,
])

ncores_list = np.array([
    32,
    64,
    128,
    256,
    512,
    1024,
])

nruns = len(ncores_list)

eff_list   = np.zeros(nruns)


for i in range(nruns):

    ncells     = ncells_list[i]
    nsteps     = nsteps_list[i]
    nsec       = nsec_list[i]
    ncores     = ncores_list[i]

    # runtime efficiency
    eff = RE(ncells, nsteps, nsec, ncores)

    eff_list[i]  = eff


# secs_per_1000steps = ncells_list * 1000 / (ncores_list * eff_list)

speedup = nsteps_list / nsteps_list[0]



fig=plt.figure(1)
fig.clf()

ax  = fig.add_subplot(111)

ax.set_title('Strong Scaling Lean')
ax.set_xlabel('#cpu')
# ax.set_ylabel('Efficiency')
# ax.set_ylabel('#steps')
ax.set_ylabel('Speedup normalized to 32 cores')

# ax.set_xlim([0,120])
# ax.set_ylim([-2.1e-6,2.1e-6])

# ax.plot(ncores_list, eff_list, 'x-')
# ax.plot(ncores_list, nsteps_list, 'rx')
ax.plot(ncores_list, speedup, 'r--x', label="speedup")
ax.plot(ncores_list, ncores_list / ncores_list[0], 'k-', label="ideal")

ax.legend(loc='upper left')

dirout  = './'
fileout = 'Lean_strong_scaling_2'
fig.savefig(dirout + '/' + fileout + '.pdf')

# np.savetxt(dirout + '/' + 'strong.dat', np.array([ncores_list, eff_list]).T )
np.savetxt(dirout + '/' + 'strong.dat', np.array([ncores_list, nsteps_list, speedup]).T )

plt.show()
