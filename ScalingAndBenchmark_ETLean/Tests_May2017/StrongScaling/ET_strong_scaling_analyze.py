#!/usr/bin/env python

from __future__ import division
from __future__ import print_function

import numpy as np

import matplotlib
import matplotlib.pyplot as plt 


def RE(ntotcells, nsteps, nsec, ncores):
    return 1.0 * ntotcells * nsteps / (nsec * ncores)

ncells_list = [
    28652616 + 2617972 + 2883307 + 3413977 + 4475317 + 4581451 + 2506891,
    28652616 + 2617972 + 2883307 + 3413977 + 4475317 + 4581451 + 2506891,
    28652616 + 2617972 + 2883307 + 3413977 + 4475317 + 4581451 + 2506891,
    28652616 + 2617972 + 2883307 + 3413977 + 4475317 + 4581451 + 2506891,
    28652616 + 2617972 + 2883307 + 3413977 + 4475317 + 4581451 + 2506891,
]

nsteps_list = [
    457,
    801,
    1529,
    2345,
    3900,
]

nsec_list = [
    3600,
    3600,
    3600,
    3600,
    3600,
]

ncores_list = [
    32,
    64,
    128,
    256,
    512,
]

nruns = len(ncores_list)

eff_list   = np.zeros(nruns)


for i in range(nruns):

    ncells     = ncells_list[i]
    nsteps     = nsteps_list[i]
    nsec       = nsec_list[i]
    ncores     = ncores_list[i]

    # runtime efficiency
    eff = RE(ncells, nsteps, nsec, ncores)

    eff_list[i]  = eff



fig=plt.figure(1)
fig.clf()

ax  = fig.add_subplot(111)

ax.set_title('Strong Scaling')
ax.set_xlabel('#cpu')
ax.set_ylabel('Efficiency')

# ax.set_xlim([0,120])
# ax.set_ylim([-2.1e-6,2.1e-6])

ax.plot(ncores_list, eff_list, 'x-')

dirout  = './'
fileout = 'ET_strong_scaling'
fig.savefig(dirout + '/' + fileout + '.pdf')

# np.savetxt(dirout + '/' + 'strong.dat', np.array([ncores_list, eff_list]).T )

plt.show()
