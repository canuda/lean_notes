# Scalar ASCII output created by CarpetIOScalar
# created on cosmos2.private.damtp.cam.ac.uk by hw401 on May 21 2017 at 11:51:15+0100
# parameter filename: "Lean_head-on_d06_hf160_strong_np128.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 946 0 848 0 0 32 790 59 48 0
256 0.8 988 0 848 0 0 32 790 59 48 0
512 1.6 997 0 848 0 0 32 790 59 48 0
768 2.4 1001 0 848 0 0 32 790 59 48 0
1024 3.2 1003 0 848 0 0 32 790 59 48 0
1280 4 1004 0 848 0 0 32 790 59 48 0
1536 4.8 1004 0 848 0 0 32 790 59 48 0
1792 5.6 1004 0 848 0 0 32 790 59 48 0
