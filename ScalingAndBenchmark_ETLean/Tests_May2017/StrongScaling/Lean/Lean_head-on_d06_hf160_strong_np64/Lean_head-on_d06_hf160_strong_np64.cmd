#!/bin/bash

### ============ Setup ==============================

#PBS -q large2
#PBS -V 
#PBS -j oe 
#PBS -N Lean_head-on_d06_hf160_strong_np64
#PBS -l nodes=8:ppn=8
#PBS -l walltime=01:00:00

###PBS -q large2@cosmos2
###PBS -l mem=24GB

### ================================================

### ========= shell commands =======================
date

### ================================================

### ========= Execution ============================

CACTUS=/home/cosmos/users/hw401/Code/ET_Lean/Cactus/exe/cactus_Lean_WBSSN
RUNNAME=Lean_head-on_d06_hf160_strong_np64
RUNPATH=/fast/space/projects/lean2013/hw401/ScalingAndBenchmark_ETLean/StrongScaling/Lean


cd $RUNPATH
NP=$PBS_NP

mpiexec_mpt -np $NP dplace -s1 $CACTUS $RUNNAME.par >& $RUNNAME.log

echo Done.

### ================================================
