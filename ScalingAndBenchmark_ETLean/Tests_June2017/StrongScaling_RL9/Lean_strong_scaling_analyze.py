#!/usr/bin/env python

from __future__ import division
from __future__ import print_function

import numpy as np

import matplotlib
import matplotlib.pyplot as plt 


def RE(ntotcells, nsteps, nsec, ncores):
    return 1.0 * ntotcells * nsteps / (nsec * ncores)

# number of cores
ncores_list = np.array([
        256,
#        384,
        512,
        768,
        1024,
])

# velocity of sim in M / s
vel_list = np.array([
#256,
#        0.699219        # AllTimer
        0.8202080,
#384,
#        2.06836         # AllTimer
#        2.2894290
#512,
#        1.00195         # AllTimer
        1.2228613,
#768,
        3.36133,         # AllTimer
#        3.7370246,
#1024,
#        3.375           # AllTimer
        3.7776353,
])

# iterations
nsteps_list = np.array([
#256,
        358,
#384,
#        1059
#512,
        513,
#768,
        1721,
#1024,
        1728,
])

# number of grid points 
ncells_list = np.array([
#256,
        17779581 + 21250625 + 21704375 + 22611875 + 24426875 + 28056875 + 35316875 + 20796875 + 20796875 + 20796875 + 20796875,
#384,
#        17779581 + 21250625 + 21704375 + 22611875 + 24426875 + 28056875 + 35316875 + 20796875 + 20796875 + 20796875 + 20796875,
#512,
        17779581 + 21250625 + 21704375 + 22611875 + 24426875 + 28056875 + 35316875 + 20796875 + 20796875 + 20796875 + 20796875,
#768,
        17779581 + 21250625 + 21704375 + 22611875 + 24426875 + 28056875 + 35316875 + 20796875 + 20796875 + 20796875 + 20796875,
#1024,
        17779581 + 21250625 + 21704375 + 22611875 + 24426875 + 28056875 + 35316875 + 20796875 + 20796875 + 20796875 + 20796875,
])

nsec_list = np.array([
    7200,
    7200,
    7200,
    7200,
    7200,
])


nruns = len(ncores_list)

eff_list   = np.zeros(nruns)


for i in range(nruns):

    ncells     = ncells_list[i]
    nsteps     = nsteps_list[i]
    nsec       = nsec_list[i]
    ncores     = ncores_list[i]
    vel        = vel_list[i]

    # runtime efficiency
    eff = RE(ncells, nsteps, nsec, ncores)

    eff_list[i]  = eff


# secs_per_1000steps = ncells_list * 1000 / (ncores_list * eff_list)

#speedup = nsteps_list / nsteps_list[0]
speedup = vel_list / vel_list[0]


fig=plt.figure(1)
fig.clf()

ax  = fig.add_subplot(111)

ax.set_title('Strong Scaling Lean')
ax.set_xlabel('#cpu')
# ax.set_ylabel('Efficiency')
# ax.set_ylabel('#steps')
ax.set_ylabel('Speedup normalized to 256 cores')
#ax.set_ylabel('Speedup normalized to 'ncores_list[0]' cores')

# ax.set_xlim([0,120])
# ax.set_ylim([-2.1e-6,2.1e-6])

# ax.plot(ncores_list, eff_list, 'x-')
# ax.plot(ncores_list, nsteps_list, 'rx')
ax.plot(ncores_list, speedup, 'r--x', label="speedup")
ax.plot(ncores_list, ncores_list / ncores_list[0], 'k-', label="ideal")

ax.legend(loc='upper left')

dirout  = './'
fileout = 'Lean_strong_scaling_2'
fig.savefig(dirout + '/' + fileout + '.pdf')

# np.savetxt(dirout + '/' + 'strong.dat', np.array([ncores_list, eff_list]).T )
#np.savetxt(dirout + '/' + 'strong.dat', np.array([ncores_list, nsteps_list, speedup]).T )
np.savetxt(dirout + '/' + 'strong.dat', np.array([ncores_list, vel_list, speedup]).T )

plt.show()
