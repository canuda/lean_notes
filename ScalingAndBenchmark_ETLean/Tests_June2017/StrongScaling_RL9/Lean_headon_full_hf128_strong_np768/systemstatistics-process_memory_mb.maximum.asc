# Scalar ASCII output created by CarpetIOScalar
# created on cosmos2.private.damtp.cam.ac.uk by hw401 on Jun 21 2017 at 09:48:17+0100
# parameter filename: "Lean_headon_full_hf128_strong_np768.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 1283 0 876 0 0 192 851 30 14 0
128 0.5 1405 0 876 0 0 192 851 80 56 0
256 1 1556 0 876 0 0 192 851 99 74 0
384 1.5 1687 0 876 0 0 192 851 99 74 0
512 2 1796 0 876 0 0 192 851 99 74 0
640 2.5 1884 0 876 0 0 192 851 99 74 0
768 3 1958 0 876 0 0 192 851 99 74 0
896 3.5 2019 0 876 0 0 192 851 99 74 0
1024 4 2064 0 876 0 0 192 851 99 74 0
1152 4.5 2099 0 876 0 0 192 851 99 74 0
1280 5 2128 0 876 0 0 192 851 99 74 0
1408 5.5 2152 0 876 0 0 192 851 99 74 0
1536 6 2173 0 876 0 0 192 851 99 74 0
1664 6.5 2187 0 876 0 0 192 851 99 74 0
