#!/usr/bin/env python

from __future__ import division
from __future__ import print_function

import numpy as np

import matplotlib
import matplotlib.pyplot as plt 


def RE(ntotcells, nsteps, nsec, ncores):
    return 1.0 * ntotcells * nsteps / (nsec * ncores)



ncores_list = np.array([
        256,
        384,
        512,
        768,
       1024,
])

#velocity in M/s
run_vel = np.array([
# 256
        2.9213138,
# 384
        1.7827602,
# 512
        2.8935422,
# 768
        2.6194315,
# 1024
        1.1163143,
])


ncells_list = np.array([
# 256
        81182737 + 13735117 + 14820897 + 16992457 + 21335577 + 12649337 + 12649337 + 12649337 + 12649337,
# 384
        104487111 + 17199273 + 18585295 + 21357339 + 26901427 + 15876252 + 15876252 + 15813251 + 15813251,
# 512
        138188413 + 22611875 + 24426875 + 28056875 + 35316875 + 20796875 + 20796875 + 20796875 + 20796875,
# 768
        190109375 + 30388779 + 32775813 + 37733499 + 47465253 + 27909936 + 27909936 + 27909936 + 27909936,
# 1024
        268336125 + 42405849 + 45853479 + 52748739 + 66539259 + 38958219 + 38958219 + 38958219 + 38958219,
])

nsteps_list = np.array([
# 256
        1129,
# 384
         737,
# 512
        1338,
# 768
        1337,
# 1024
        572,
])

nsec_list = np.array([
    7200,
    7200,
    7200,
    7200,
    7200,
])


nruns = len(ncores_list)

eff_list   = np.zeros(nruns)


for i in range(nruns):

    ncells     = ncells_list[i]
    nsteps     = nsteps_list[i]
    nsec       = nsec_list[i]
    ncores     = ncores_list[i]

    # runtime efficiency
    eff = RE(ncells, nsteps, nsec, ncores)

    eff_list[i]  = eff


secs_per_1000steps = ncells_list * 1000 / (ncores_list * eff_list)



fig=plt.figure(1)
fig.clf()

ax  = fig.add_subplot(111)

ax.set_title('Weak Scaling Lean')
ax.set_xlabel('#cpu')
ax.set_ylabel('Time (secs)')

# ax.set_xlim([0,120])
# ax.set_ylim([-2.1e-6,2.1e-6])

# ax.plot(ncores_list, eff_list, 'x-')
ax.plot(ncores_list, secs_per_1000steps, 'x-')

dirout  = './'
fileout = 'Lean_weak_scaling'
fig.savefig(dirout + '/' + fileout + '.pdf')

np.savetxt(dirout + '/' + 'weak.dat', np.array([ncores_list,secs_per_1000steps]).T )

plt.show()
