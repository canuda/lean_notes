\documentclass[12pt,a4paper,titlepage,oneside,nofootinbib]{article}
\usepackage{graphicx, epsfig, amssymb} %include figure files
\usepackage{amsmath, amsfonts}
\usepackage{bm} %include bold math: \bm{} creates bold letters in math mode
\graphicspath{{Figures/}}
% use fancy font required in the template
% for fontspec: need to run XeLatex (or LuaLaTex)
% \usepackage{fontspec}
 % \newfontfamily\myfont[Mapping=tex-text]{Verdana}
% \setmainfont[Mapping=tex-text]{Verdana}

% define page layout
\usepackage{layout}
% lhs/top: 1 inch + \hoffset or 1 inch + \voffset; recall 1 inch = 2.54cm
%\voffset       = - 0.54cm
\hoffset       = - 0.54cm
\topmargin     = - 1.25cm
\headheight    =   0.50cm
\headsep       =   0.20cm
\marginparsep  =   0.00cm
\oddsidemargin = - 0.00cm
\marginparwidth=   1.75cm
\marginparpush =   0.00cm
\textwidth     =  17.00cm
\textheight    =  23.25cm
\footskip      =   0.75cm
%\textwidth     =  18.00cm %for letter size 
%\textheight    =  23.50cm %for letter size
\setlength{\headheight}{1.25cm}


% headings and footings
\usepackage{fancyhdr}
\usepackage{lastpage}
\pagestyle{fancy}
\lhead{{\textbf{PRACE application form}}
\\
Project scope and plan
}
\chead{}
\rhead{{\textbf{
15$^{th}$ Call for PRACE Project Access
}}}
\lfoot{
PRACE AISBL\\
Rue du Tr\^{o}ne 98,\\
B-1050 Bruxelles
}
\cfoot{
Page \thepage \hspace{1pt} / \pageref{LastPage}
}
\rfoot{
https://prace-peer-review.cines.fr/
}
\renewcommand{\footrulewidth}{0.5pt}

% packages for special table environments and features
\usepackage{tabularx}
\usepackage{multirow}
%\usepackage{colortbl}


% additional packages
\usepackage[gen]{eurosym}
\usepackage{enumitem}
\usepackage{pdflscape}
%\usepackage{scrextend}
\usepackage[linktocpage,hyperfootnotes=false]{hyperref}
\usepackage[multiple]{footmisc}
\usepackage[caption=false]{subfig}
%\usepackage[usenames]{color}
\usepackage[usenames,table]{xcolor}

\newlength{\bibitemsep}\setlength{\bibitemsep}{.2\baselineskip plus .05\baselineskip minus .05\baselineskip}
\newlength{\bibparskip}\setlength{\bibparskip}{0pt}
\let\oldthebibliography\thebibliography
\renewcommand\thebibliography[1]{%
  \oldthebibliography{#1}%
    \setlength{\parskip}{\bibitemsep}%
      \setlength{\itemsep}{\bibparskip}%
}


% additional definitions and commands
\newcommand{\EL}[1]{\textcolor{blue}{[{\it\textbf{EL: #1}}]} }
\newcommand{\HW}[1]{\textcolor{red}{[{\it\textbf{HW: #1}}]} }
\newcommand{\MZ}[1]{\textcolor{green}{[{\it\textbf{MZ: #1}}]} }

%\newcommand{\staritem}{\stepcounter{enumi}\item[$*$\theenumi.]}
\newcommand{\staritem}{\stepcounter{enumi}\item[\theenumi.$^*$]}
\newcommand{\nostaritem}{\stepcounter{enumi}\item[\theenumi.\,\,]}
\newcommand{\mpl}{M_{\mathrm{pl}}}


%definition of abbreviations
\def\Lie{\mathcal{L}}
\def\p{\partial}
\def\non{\nonumber}
\def\half{{\textstyle{1\over2}}}
\def\ss{\scriptscriptstyle}
%
\renewcommand{\vec}[1]{\boldsymbol{#1}}


\setcounter{tocdepth}{2}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start document
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% check layout
%\layout      % with package ``layout''
%\currentpage % with package ``layouts''
%\pagedesign  % with package ``layouts''
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\footskip = 20pt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title page
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Scaling tests: \textsc{Einstein Toolkit} -- \textsc{Lean}}
%==========================================

{\textsc{Lean}} is based on the {\textsc{Einstein Toolkit}}, which employs the Cactus Computational Toolkit
and uses the Carpet package for the block-structured AMR infrastructure. 
Each AMR box is its own independent domain, which is  distributed to different processing nodes via MPI. 
This is a crucial ingredient to resolve the very different length scales arising in typical evolutions.

Our scaling tests are based on a set of simulations evolving the early stages of a BH head-on collision from rest with initial
separation of $6M$. 
Such a configuration is comparable to typical runs that we will perform in the context of the present proposal and it has the 
further advantage of the initial data generation being trivial. The tests were performed on the {\em Cosmos} cluster, a SGI UV2000 system 
that features 1,856 Intel Xeon E5 (Sandy Bridge) cores with 14.5TB of ccNUMA globally shared memory via the NUMAlink 6 interconnect. 

For the tests reported here
%we used a grid setup of the type $\{(120, 32, 16, 8, 4) \times (1.6, 0.6), h = hf\}$.
our computational domain consists of a three-dimensional nested grid with $9$ refinement levels and whose outermost mesh has a box size of $256M$. We do not assume any symmetries. 
The seven outer levels are fixed and 
consist of one mesh, whereas the two inner levels contain $2$ meshes centered around the individual BHs. The refinement factor between subsequent levels is $2$. 
%$h_f$ indicates the resolution of the innermost refinement level.

We performed the strong scaling test by varying the number of cores for a fixed problem size.
Specifically we fix the grid setup above and set the resolution of the inner level to $h_{f} = 1/128$, i.e., using $160$ points in each direction. Accounting for the nested grid structure, this amounts to a total number of gridpoints of about $3\cdot10^{8}$.
We evolved this configuration for $2$ hour of physical time. We show the results in Table~\ref{tab:TableStrongScaling}, where we present the velocity of the simulation in terms of evolved time per hour,
and in Fig.~\ref{fig:StrongScalingLean}, where we present the speedup as compared to the simulation using $256$ cores.
%
We overall observe good scaling for $1024$ cores, which is the maximum number of cores that we could use for these tests on {\em Cosmos}.
The weak scaling test shows similarly good behaviour.
We perform the corresponding test by varying the resolution $h_{f}$ (while keeping the grid setup fixed) 
and number of cores so that the number of points per core is approximately constant. 
As expected for a weak scaling test, the execution time remains approximately constant as we increase the number of processors.

%==========================================
% Tables and figures
%==========================================

\begin{figure}[htpb!]
\includegraphics[width=0.50\textwidth,height=0.345\textwidth,clip]{Lean_strong_scaling_2.pdf}
\caption{\label{fig:StrongScalingLean}
 Strong scaling tests
}
\end{figure}


\begin{table}[htpb!]
\begin{center}
\caption{\label{tab:TableStrongScaling}
Strong scaling for {\textsc{LEAN}}
}
\begin{tabular}{|c|c|c|}
\hline
\parbox{2.00cm}{\# cores } &
\parbox{2.00cm}{M / h} &
\parbox{2.00cm}{speedup } \\
\hline
\hline
256  & 0.82     & 1.00
\\ \hline
512  & 1.22     & 1.49
\\ \hline
768  & 3.36     & 4.09
\\ \hline
1024 & 3.78     & 4.61
\\ \hline
\end{tabular}
\end{center}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\clearpage
%\printbibliography
\bibliographystyle{h-physrev4}
%\renewcommand\thebibliography[1]{
%    \OLDthebibliography{#1}
 %     \setlength{\parskip}{0pt}
 %       \setlength{\itemsep}{0pt plus 0.3ex}
\bibliography{Prace_bib}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Appendix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\clearpage
%\newpage
%\appendix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{CV of the Principal Investigator with list of publications}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%==========================================

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% End document
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
