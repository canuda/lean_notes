# Scalar ASCII output created by CarpetIOScalar
# created on cosmos2.private.damtp.cam.ac.uk by hw401 on Jun 21 2017 at 17:35:17+0100
# parameter filename: "Lean_headon_full_hf116_weak_np768.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 936 0 604 0 0 192 580 14 0 0
128 0.55 955 0 604 0 0 192 580 14 0 0
256 1.1 956 0 604 0 0 192 580 14 0 0
384 1.65 958 0 604 0 0 192 580 14 0 0
