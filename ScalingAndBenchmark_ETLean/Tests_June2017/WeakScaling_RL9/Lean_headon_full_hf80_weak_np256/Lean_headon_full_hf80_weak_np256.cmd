#!/bin/bash

### ============ Setup ==============================

#PBS -q large2
#PBS -V 
#PBS -j oe 
#PBS -N Lean_headon_full_hf80_weak_np256
#PBS -l nodes=32:ppn=8
#PBS -l walltime=02:00:00

###PBS -q large2@cosmos2
###PBS -l mem=24GB

### ================================================

### ========= shell commands =======================
date

### ================================================

### ========= Execution ============================

CACTUS=/home/cosmos/users/hw401/Code/ET_Lean/Cactus/exe/cactus_Lean_WBSSN
RUNNAME=Lean_headon_full_hf80_weak_np256
RUNPATH=/fast/space/projects/lean2013/hw401/ScalingAndBenchmark_ETLean/Tests_June2017/WeakScaling_RL9


cd $RUNPATH
NP=$PBS_NP

mpiexec_mpt -np $NP dplace -s1 $CACTUS $RUNNAME.par >& $RUNNAME.log

echo Done.

### ================================================
