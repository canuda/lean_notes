# Scalar ASCII output created by CarpetIOScalar
# created on cosmos2.private.damtp.cam.ac.uk by hw401 on Jun 21 2017 at 12:35:47+0100
# parameter filename: "Lean_headon_full_hf80_weak_np256.par"
#
# SYSTEMSTATISTICS::maxrss_mb (systemstatistics-process_memory_mb)
# 1:iteration 2:time 3:data
# data columns: 3:maxrss_mb 4:majflt_mb 5:arena_mb 6:ordblks_mb 7:hblks_mb 8:hblkhd_mb 9:uordblks_mb 10:fordblks_mb 11:keepcost_mb 12:swap_used_mb
0 0 803 0 664 0 0 64 638 18 0 0
128 0.775 813 0 664 0 0 64 638 18 0 0
256 1.55 814 0 664 0 0 64 638 18 0 0
384 2.325 815 0 664 0 0 64 638 18 0 0
512 3.1 815 0 664 0 0 64 638 18 0 0
640 3.875 815 0 664 0 0 64 638 18 0 0
768 4.65 815 0 664 0 0 64 638 18 0 0
896 5.425 815 0 664 0 0 64 638 18 0 0
1024 6.2 815 0 664 0 0 64 638 18 0 0
1152 6.975 815 0 664 0 0 64 638 18 0 0
1280 7.75 815 0 664 0 0 64 638 18 0 0
1408 8.525 815 0 664 0 0 64 638 18 0 0
1536 9.3 816 0 664 0 0 64 638 18 0 0
1664 10.075 816 0 664 0 0 64 638 18 0 0
1792 10.85 816 0 664 0 0 64 638 18 0 0
1920 11.625 816 0 664 0 0 64 638 18 0 0
2048 12.4 816 0 664 0 0 64 638 18 0 0
