################################################################################
# Active thorns
################################################################################

ActiveThorns = "
ADMBase
#ML_ADMConstraints
ADMCoupling
ADMMacros
AEILocalInterp
AHFinderDirect
Boundary
Carpet
CarpetIOASCII
CarpetIOBasic
CarpetIOHDF5
CarpetIOScalar
CarpetInterp
CarpetInterp2
CarpetLib
CarpetMask
CarpetReduce
CarpetRegrid2
CarpetTracker
CartGrid3D
CoordBase
CoordGauge
Coordinates
CoordinatesSymmetry
Dissipation
Formaline
GlobalDerivative
hwloc
IOUtil
InitBase
Interpolate2
QuasiLocalMeasures
LocalInterp
LoopControl
MoL
NaNChecker
NPScalars
PunctureTracker
Slab
SpaceMask
SphericalSurface
StaticConformal
# SummationByParts   
SymBase
SystemStatistics
# SystemTopology
TerminationTrigger
TensorTypes
Time
TimerReport
TmunuBase
TwoPunctures
Vectors
LeanBSSNMoL
NewRad
GenericFD
WeylScal4
Multipole
#  WaveExtractCPM
ADMDerivatives
"

################################################################################
# Grid structure
################################################################################

Carpet::domain_from_multipatch          = yes
CartGrid3D::type                        = "multipatch"
CartGrid3D::set_coordinate_ranges_on    = "all maps"
Coordinates::coordinate_system          = "Thornburg04"
Coordinates::h_cartesian                = 0.6
Coordinates::h_radial                   = 0.6

Coordinates::sphere_inner_radius        = 36.0
Coordinates::sphere_outer_radius        = 180.0
Coordinates::n_angular                  = 24

Driver::ghost_size                      = 3
Coordinates::patch_boundary_size        = 3
Coordinates::additional_overlap_size    = 3
Coordinates::outer_boundary_size        = 3

CoordinatesSymmetry::reflection_z       = yes
CoordinatesSymmetry::stagger            = no
Coordinates::symmetry                   = "+z bitant" ## reflection symmetry in z
Coordinates::additional_symmetry_size   = 1
Coordinates::verbose                    = no

Time::timestep_method                   = "given"
Time::timestep				= 0.15 
Carpet::time_refinement_factors         = "[1,1,2,4,8,16,32,64,128,256,512]"

################################################################################
# Mesh refinement
################################################################################

Carpet::max_refinement_levels           = 7
CarpetRegrid2::num_centres              = 3

CarpetRegrid2::num_levels_1             =  7
CarpetRegrid2::position_x_1             = +3.0
CarpetRegrid2::radius_1[ 1]             =  9.6
CarpetRegrid2::radius_1[ 2]             =  4.8
CarpetRegrid2::radius_1[ 3]             =  2.4
CarpetRegrid2::radius_1[ 4]             =  1.2
CarpetRegrid2::radius_1[ 5]             =  0.6
CarpetRegrid2::radius_1[ 6]             =  0.3
CarpetRegrid2::movement_threshold_1     =  0.16

CarpetRegrid2::num_levels_2             =  7
CarpetRegrid2::position_x_2             = -3.0
CarpetRegrid2::radius_2[ 1]             =  9.6
CarpetRegrid2::radius_2[ 2]             =  4.8
CarpetRegrid2::radius_2[ 3]             =  2.4
CarpetRegrid2::radius_2[ 4]             =  1.2
CarpetRegrid2::radius_2[ 5]             =  0.6
CarpetRegrid2::radius_2[ 6]             =  0.3
CarpetRegrid2::movement_threshold_2     =  0.16

CarpetRegrid2::num_levels_3             =  7
CarpetRegrid2::active_3                 =  no
CarpetRegrid2::radius_3[ 1]             =  9.6
CarpetRegrid2::radius_3[ 2]             =  4.8
CarpetRegrid2::radius_3[ 3]             =  2.4
CarpetRegrid2::radius_3[ 4]             =  1.2
CarpetRegrid2::radius_3[ 5]             =  0.6
CarpetRegrid2::radius_3[ 6]             =  0.3
CarpetRegrid2::movement_threshold_3     =  0.16




Carpet::use_buffer_zones                = yes
Carpet::prolongation_order_space        = 5
Carpet::prolongation_order_time         = 2

CarpetRegrid2::regrid_every             = 64
CarpetRegrid2::freeze_unaligned_levels  = yes
CarpetRegrid2::verbose                  = no

Carpet::grid_structure_filename         = "carpet-grid-structure"   ###
Carpet::grid_coordinates_filename       = "carpet-grid-coordinates" ### 
################################################################################
# Initial data
################################################################################

ADMBase::initial_data                   = "twopunctures"
ADMBase::initial_lapse                  = "psi^n"  ###
ADMBase::initial_shift                  = "zero"
ADMBase::initial_dtlapse                = "zero"
ADMBase::initial_dtshift                = "zero"

ADMBase::lapse_timelevels               = 3   
ADMBase::shift_timelevels               = 3 
ADMBase::metric_timelevels              = 3 

TwoPunctures::initial_lapse_psi_exponent = -2.0 ###

TwoPunctures::give_bare_mass            = yes

TwoPunctures::target_M_plus             = 0.5
TwoPunctures::target_M_minus            = 0.5

TwoPunctures::par_m_plus              = 0.476534633024028
TwoPunctures::par_m_minus             = 0.476534633024028

TwoPunctures::par_b                     = 3.0
TwoPunctures::center_offset[0]          = 0.0

TwoPunctures::par_P_plus[0]           = -0.0058677669328272
TwoPunctures::par_P_plus[1]           = 0.138357448824906
TwoPunctures::par_P_plus[2]           = 0.

TwoPunctures::par_P_minus[0]          = 0.0058677669328272
TwoPunctures::par_P_minus[1]          = -0.138357448824906
TwoPunctures::par_P_minus[2]          = 0.


TwoPunctures::par_S_plus[0]             = 0.0
TwoPunctures::par_S_plus[1]             = 0.0
TwoPunctures::par_S_plus[2]             = 0.0

TwoPunctures::par_S_minus[0]            = 0.0
TwoPunctures::par_S_minus[1]            = 0.0
TwoPunctures::par_S_minus[2]            = 0.0

TwoPunctures::grid_setup_method         = "evaluation"
TwoPunctures::TP_epsilon                = 1e-6
TwoPunctures::TP_Tiny                   = 1.0d-10 ###

TwoPunctures::npoints_A                 = 30      ###
TwoPunctures::npoints_B                 = 30         ###
TwoPunctures::npoints_phi               = 16         ###



TwoPunctures::keep_u_around             = yes   ###
TwoPunctures::verbose                   = yes   ###


Carpet::init_fill_timelevels            = yes
InitBase::initial_data_setup_method     = "init_some_levels"
Carpet::init_3_timelevels               = no  ###


################################################################################
# Evolution and boundary
################################################################################

GenericFD::jacobian_group = "Coordinates::jacobian"
GenericFD::jacobian_derivative_group = "Coordinates::jacobian2"
GenericFD::jacobian_identity_map = 0



ADMBase::evolution_method         = "LeanBSSNMoL"
ADMBase::lapse_evolution_method   = "LeanBSSNMoL"
ADMBase::shift_evolution_method   = "LeanBSSNMoL"
ADMBase::dtlapse_evolution_method = "LeanBSSNMoL"
ADMBase::dtshift_evolution_method = "LeanBSSNMoL"

LeanBSSNMoL::derivs_order             = 4   

LeanBSSNMoL::impose_conf_fac_floor_at_initial  = yes

LeanBSSNMoL::conf_fac_floor               = 1.0d-04  ###
LeanBSSNMoL::eta_beta                     = 1        ###
LeanBSSNMoL::eta_beta_dynamic             = no
LeanBSSNMoL::use_advection_stencils       = yes      ###
LeanBSSNMoL::calculate_constraints        = yes      ###


LeanBSSNMoL::z_is_radial                 = "yes"   ### Required for Llama

################################################################################
# BH tracking
################################################################################

CarpetTracker::surface                      [0] = 0
CarpetTracker::surface                      [1] = 1
PunctureTracker::track                      [0] = yes
PunctureTracker::initial_x                  [0] = 3.0
PunctureTracker::which_surface_to_store_info[0] = 0
PunctureTracker::track                      [1] = yes
PunctureTracker::initial_x                  [1] = -3.0
PunctureTracker::which_surface_to_store_info[1] = 1
PunctureTracker::verbose                        = no 
################################################################################
# Spatial finite differencing
################################################################################

Dissipation::epsdis = 0.15
Dissipation::order  = 5
Dissipation::vars   = "
ADMBase::lapse
ADMBase::shift
LeanBSSNMoL::conf_fac
LeanBSSNMoL::hmetric
LeanBSSNMoL::hcurv
LeanBSSNMoL::trk
LeanBSSNMoL::gammat
"

################################################################################
# Time integration
################################################################################

MoL::ODE_Method                         = "rk4"
MoL::MoL_Intermediate_Steps             = 4
MoL::MoL_Num_Scratch_Levels             = 1


Carpet::num_integrator_substeps = 4
################################################################################
# Interpolation
################################################################################

CarpetInterp::check_tree_search         = no
CarpetInterp::tree_search               = yes
# Use 5-th order interpatch interpolation on the Llama grid
Interpolate::interpolator_order         = 5

################################################################################
# Psi4 computation
################################################################################
WeylScal4::fdOrder      = 4
NPScalars::NP_order     = 4
##WeylScal4::calc_scalars              = "psis"
##WeylScal4::calc_invariants           = "always"

################################################################################
# Psi4 mode decomposition
################################################################################

# Radii are chosen to be evenly spaced in 1/r as that is the variable
# extrapolation is performed in
Multipole::nradii       = 4
Multipole::radius[0]    = 50
Multipole::radius[1]    = 75
Multipole::radius[2]    = 100
Multipole::radius[3]    = 150
Multipole::ntheta       = 120  ### 120
Multipole::nphi         = 240  ### 240
Multipole::variables    = "
NPScalars::psi4re{sw=-2 cmplx='NPScalars::psi4im' name='Psi4'}
WeylScal4::Psi4r{sw=-2 cmplx='WeylScal4::Psi4i' name='psi4'}
"


## Output
Multipole::out_every    = 32
Multipole::l_max        = 6 ### 8
Multipole::output_hdf5  = no
Multipole::output_ascii = yes
Multipole::integration_method = "Simpson" ###

################################################################################
# Apparent Horizons
################################################################################

AHFinderDirect::output_BH_diagnostics                    = "true"
AHFinderDirect::run_at_CCTK_POST_RECOVER_VARIABLES       = no


AHFinderDirect::N_horizons                               = 3
AHFinderDirect::find_every                               = 128
AHFinderDirect::output_h_every                           = 0
AHFinderDirect::max_Newton_iterations__initial           = 50
AHFinderDirect::max_Newton_iterations__subsequent        = 50
AHFinderDirect::max_allowable_Theta_growth_iterations    = 10
AHFinderDirect::max_allowable_Theta_nonshrink_iterations = 10
AHFinderDirect::geometry_interpolator_name               = "Lagrange polynomial interpolation"
AHFinderDirect::geometry_interpolator_pars               = "order=4"
AHFinderDirect::surface_interpolator_name                = "Lagrange polynomial interpolation"
AHFinderDirect::surface_interpolator_pars                = "order=4"
AHFinderDirect::verbose_level                            = "physics details"
AHFinderDirect::move_origins                             = yes


AHFinderDirect::reshape_while_moving                     = yes ###
AHFinderDirect::predict_origin_movement                  = yes ###


AHFinderDirect::origin_x                             [1] = 3.0
AHFinderDirect::initial_guess__coord_sphere__x_center[1] = 3.0
AHFinderDirect::initial_guess__coord_sphere__radius  [1] = 0.25
AHFinderDirect::which_surface_to_store_info          [1] = 0
AHFinderDirect::set_mask_for_individual_horizon      [1] = no
AHFinderDirect::reset_horizon_after_not_finding      [1] = no
AHFinderDirect::track_origin_from_grid_scalar        [1] = yes
AHFinderDirect::track_origin_source_x                [1] = "PunctureTracker::pt_loc_x[0]"
AHFinderDirect::track_origin_source_y                [1] = "PunctureTracker::pt_loc_y[0]"
AHFinderDirect::track_origin_source_z                [1] = "PunctureTracker::pt_loc_z[0]"
AHFinderDirect::max_allowable_horizon_radius         [1] = 3

AHFinderDirect::origin_x                             [2] = -3.0
AHFinderDirect::initial_guess__coord_sphere__x_center[2] = -3.0
AHFinderDirect::initial_guess__coord_sphere__radius  [2] = 0.25
AHFinderDirect::which_surface_to_store_info          [2] = 1
AHFinderDirect::set_mask_for_individual_horizon      [2] = no
AHFinderDirect::reset_horizon_after_not_finding      [2] = no
AHFinderDirect::track_origin_from_grid_scalar        [2] = yes
AHFinderDirect::track_origin_source_x                [2] = "PunctureTracker::pt_loc_x[1]"
AHFinderDirect::track_origin_source_y                [2] = "PunctureTracker::pt_loc_y[1]"
AHFinderDirect::track_origin_source_z                [2] = "PunctureTracker::pt_loc_z[1]"
AHFinderDirect::max_allowable_horizon_radius         [2] = 3

AHFinderDirect::origin_x                             [3] = 0
AHFinderDirect::find_after_individual                [3] = 50.0
AHFinderDirect::initial_guess__coord_sphere__x_center[3] = 0
AHFinderDirect::initial_guess__coord_sphere__radius  [3] = 1.0
AHFinderDirect::which_surface_to_store_info          [3] = 2
AHFinderDirect::set_mask_for_individual_horizon      [3] = no
AHFinderDirect::max_allowable_horizon_radius         [3] = 6

################################################################################
# Spherical surfaces
################################################################################

SphericalSurface::nsurfaces             = 3
SphericalSurface::maxntheta             = 66
SphericalSurface::maxnphi               = 124
SphericalSurface::verbose               = no

# Surfaces 0 and 1 are used by PunctureTracker

# Horizon 1
SphericalSurface::ntheta            [0] = 41
SphericalSurface::nphi              [0] = 80
SphericalSurface::nghoststheta      [0] = 2
SphericalSurface::nghostsphi        [0] = 2
CarpetMask::excluded_surface        [0] = 0
CarpetMask::excluded_surface_factor [0] = 1.0


# Horizon 2
SphericalSurface::ntheta            [1] = 41
SphericalSurface::nphi              [1] = 80
SphericalSurface::nghoststheta      [1] = 2
SphericalSurface::nghostsphi        [1] = 2
CarpetMask::excluded_surface        [1] = 1
CarpetMask::excluded_surface_factor [1] = 1.0

# Common horizon
SphericalSurface::ntheta            [2] = 41
SphericalSurface::nphi              [2] = 80
SphericalSurface::nghoststheta      [2] = 2
SphericalSurface::nghostsphi        [2] = 2
CarpetMask::excluded_surface        [2] = 2
CarpetMask::excluded_surface_factor [2] = 1.0  

CarpetMask::verbose = no

################################################################################
# Isolated Horizons
################################################################################

QuasiLocalMeasures::verbose                = yes
QuasiLocalMeasures::veryverbose            = no
QuasiLocalMeasures::interpolator           = "Lagrange polynomial interpolation"
QuasiLocalMeasures::interpolator_options   = "order=4"
QuasiLocalMeasures::spatial_order          = 4
QuasiLocalMeasures::num_surfaces           = 3
QuasiLocalMeasures::surface_index      [0] = 0
QuasiLocalMeasures::surface_index      [1] = 1
QuasiLocalMeasures::surface_index      [2] = 2


################################################################################
# Correctness checking
################################################################################

Carpet::poison_new_timelevels           = yes
Carpet::check_for_poison                = no
CarpetLib::poison_new_memory            = yes ###

NaNChecker::check_every                 = 256
NanChecker::check_after                 = 0
NaNChecker::report_max                  = 10
NaNChecker::verbose                     = "all"
NaNChecker::action_if_found             = terminate
NaNChecker::out_NaNmask                 = yes
NaNChecker::check_vars                  = "
LeanBSSNMoL::conf_fac
"

################################################################################
# Timers
################################################################################

Cactus::cctk_timer_output               = "full"
TimerReport::out_every                  = 5120
TimerReport::n_top_timers               = 40
TimerReport::output_all_timers_together = yes
TimerReport::output_all_timers_readable = yes
TimerReport::output_schedule_timers     = no

################################################################################
# Output
################################################################################

IO::out_dir                             = "LeanMulti_qc0High_Diss15_V2/Output"
IOScalar::one_file_per_group            = yes
IOASCII::one_file_per_group             = yes

IOBasic::outInfo_every                  = 10
IOBasic::outInfo_reductions             = "minimum maximum"
IOBasic::outInfo_vars                   = "
Carpet::physical_time_per_hour
SystemStatistics::maxrss_mb
SystemStatistics::swap_used_mb
"

IOScalar::outScalar_every               = 0
IOScalar::outScalar_reductions          = "minimum maximum average"
IOScalar::outScalar_vars                = "SystemStatistics::process_memory_mb"

IOASCII::out0D_every                    = 10
IOASCII::out0D_vars                     = "
Carpet::timing
PunctureTracker::pt_loc
QuasiLocalMeasures::qlm_scalars{out_every = 64}
"

IOASCII::out1D_every                    = 256
IOASCII::out1D_d                        = no
IOASCII::out1D_x                        = yes
IOASCII::out1D_y                        = no
IOASCII::out1D_z                        = yes
IOASCII::out1D_vars                     = "
ADMBase::lapse
ADMBase::metric
ADMBase::curv
LeanBSSNMoL::ham
LeanBSSNMoL::mom
"



################################################################################
# Checkpointing and recovery
################################################################################

CarpetIOHDF5::checkpoint                    = yes
IO::checkpoint_ID                           = no
IO::recover                                 = "autoprobe"
IO::out_proc_every                          = 2
IO::checkpoint_on_terminate                 = yes
IO::checkpoint_every_walltime_hours         = 8
IO::checkpoint_dir                          = "LeanMulti_qc0High_Diss15_V2/Checkpoints"
IO::recover_dir                             = "LeanMulti_qc0High_Diss15_V2/Checkpoints"
IO::abort_on_io_errors                      = yes
CarpetIOHDF5::open_one_input_file_at_a_time = yes
CarpetIOHDF5::compression_level             = 0

################################################################################
# Run termination
################################################################################

TerminationTrigger::max_walltime = 48
#Trigger termination 30 minutes before the walltime is reached
TerminationTrigger::on_remaining_walltime        = 30
TerminationTrigger::output_remtime_every_minutes = 30
TerminationTrigger::termination_from_file        = yes
TerminationTrigger::termination_file             = "terminate.txt"
TerminationTrigger::create_termination_file      = yes

Cactus::terminate                               = time
Cactus::cctk_final_time                         = 300
