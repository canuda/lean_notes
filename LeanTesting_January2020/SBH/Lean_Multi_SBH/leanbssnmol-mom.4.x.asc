# 1D ASCII output created by CarpetIOASCII
# created on i01r06c03s11.sng.lrz.de by di39wol2 on Feb 25 2020 at 03:36:50+0100
# parameter filename: "/dss/dsshome1/09/di39wol2/ET/Cactus/par/LeanTests/SBH/Lean_Multi_SBH.par"
#
# LEANBSSNMOL::MOM x (leanbssnmol-mom)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 256   time 4
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 256   time 4
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 256   time 4
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 256   time 4
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 256   time 4
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 256   time 4
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 512   time 8
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 512   time 8
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 512   time 8
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 512   time 8
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 512   time 8
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 512   time 8
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 768   time 12
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 768   time 12
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 768   time 12
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 768   time 12
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 768   time 12
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 768   time 12
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 1024   time 16
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1024   time 16
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1024   time 16
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1024   time 16
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1024   time 16
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1024   time 16
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 1280   time 20
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1280   time 20
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1280   time 20
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1280   time 20
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1280   time 20
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1280   time 20
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 1536   time 24
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1536   time 24
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1536   time 24
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1536   time 24
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1536   time 24
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1536   time 24
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 1792   time 28
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1792   time 28
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1792   time 28
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1792   time 28
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1792   time 28
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 1792   time 28
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 2048   time 32
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2048   time 32
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2048   time 32
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2048   time 32
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2048   time 32
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2048   time 32
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 2304   time 36
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2304   time 36
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2304   time 36
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2304   time 36
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2304   time 36
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2304   time 36
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 2560   time 40
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2560   time 40
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2560   time 40
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2560   time 40
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2560   time 40
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2560   time 40
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 2816   time 44
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2816   time 44
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2816   time 44
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2816   time 44
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2816   time 44
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 2816   time 44
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 3072   time 48
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3072   time 48
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3072   time 48
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3072   time 48
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3072   time 48
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3072   time 48
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 3328   time 52
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3328   time 52
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3328   time 52
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3328   time 52
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3328   time 52
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3328   time 52
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 3584   time 56
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3584   time 56
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3584   time 56
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3584   time 56
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3584   time 56
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3584   time 56
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 3840   time 60
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3840   time 60
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3840   time 60
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3840   time 60
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3840   time 60
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 3840   time 60
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 4096   time 64
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4096   time 64
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4096   time 64
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4096   time 64
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4096   time 64
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4096   time 64
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 4352   time 68
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4352   time 68
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4352   time 68
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4352   time 68
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4352   time 68
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4352   time 68
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 4608   time 72
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4608   time 72
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4608   time 72
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4608   time 72
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4608   time 72
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4608   time 72
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 4864   time 76
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4864   time 76
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4864   time 76
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4864   time 76
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4864   time 76
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 4864   time 76
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 5120   time 80
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5120   time 80
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5120   time 80
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5120   time 80
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5120   time 80
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5120   time 80
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 5376   time 84
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5376   time 84
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5376   time 84
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5376   time 84
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5376   time 84
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5376   time 84
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 5632   time 88
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5632   time 88
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5632   time 88
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5632   time 88
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5632   time 88
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5632   time 88
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 5888   time 92
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5888   time 92
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5888   time 92
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5888   time 92
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5888   time 92
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 5888   time 92
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 6144   time 96
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 6144   time 96
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 6144   time 96
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 6144   time 96
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 6144   time 96
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 6144   time 96
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






# iteration 6400   time 100
# time level 0
# refinement level 0   multigrid level 0   map 4   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 6400   time 100
# time level 0
# refinement level 0   multigrid level 0   map 4   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 6400   time 100
# time level 0
# refinement level 0   multigrid level 0   map 4   component 2
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 6400   time 100
# time level 0
# refinement level 0   multigrid level 0   map 4   component 3
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 6400   time 100
# time level 0
# refinement level 0   multigrid level 0   map 4   component 4
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#
# iteration 6400   time 100
# time level 0
# refinement level 0   multigrid level 0   map 4   component 5
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:mcx 14:mcy 15:mcz
#






