
#------------------------------------------------------------------------------
ActiveThorns = "
  ADMBase
  ADMCoupling
  ADMMacros
  AEILocalInterp
  AHFinderDirect
  Boundary
  Carpet
  CarpetInterp
  CarpetIOASCII
  CarpetIOBasic
  CarpetIOHDF5
  CarpetIOScalar
  CarpetLib
  CarpetMask
  CarpetReduce
  CarpetRegrid2
  CarpetSlab
  CarpetTracker
  CartGrid3D
  CoordBase
  CoordGauge
  Dissipation
  # Formaline
  Fortran
  GenericFD
  GSL
  HDF5
  InitBase
  IOUtil
  LeanBSSNMoL
  LocalInterp
  LoopControl
  MoL
  Multipole
  NaNChecker
  NewRad
  NPScalars
  PunctureTracker
  QuasiLocalMeasures
  ReflectionSymmetry
 # RotatingSymmetry180
  Slab
  SpaceMask
  SphericalSurface
  StaticConformal
 # SummationByParts
  SymBase
  SystemStatistics
  TerminationTrigger
  Time
  TimerReport
  TmunuBase
  TwoPunctures
 # Vectors
  WeylScal4
"
#------------------------------------------------------------------------------


# Grid setup
#------------------------------------------------------------------------------

CartGrid3D::type                     = "coordbase"
Carpet::domain_from_coordbase        = yes
CoordBase::domainsize                = "minmax"

# make sure all (xmax - xmin)/dx are integers!
CoordBase::xmin                      = -60.00
CoordBase::ymin                      = -60.00
CoordBase::zmin                      =   0.00
CoordBase::xmax                      = +60.00
CoordBase::ymax                      = +60.00
CoordBase::zmax                      = +60.00
CoordBase::dx                        =   1.00
CoordBase::dy                        =   1.00
CoordBase::dz                        =   1.00

driver::ghost_size                   = 3

CoordBase::boundary_size_x_lower     = 3
CoordBase::boundary_size_y_lower     = 3
CoordBase::boundary_size_z_lower     = 3
CoordBase::boundary_size_x_upper     = 3
CoordBase::boundary_size_y_upper     = 3
CoordBase::boundary_size_z_upper     = 3

CoordBase::boundary_shiftout_x_lower = 0
CoordBase::boundary_shiftout_y_lower = 0
CoordBase::boundary_shiftout_z_lower = 1

#CarpetRegrid2::symmetry_rotating180  = yes

ReflectionSymmetry::reflection_x     = no
ReflectionSymmetry::reflection_y     = no
ReflectionSymmetry::reflection_z     = yes
ReflectionSymmetry::avoid_origin_x   = yes
ReflectionSymmetry::avoid_origin_y   = yes
ReflectionSymmetry::avoid_origin_z   = no


# Mesh refinement
#------------------------------------------------------------------------------

Carpet::max_refinement_levels           = 6

CarpetRegrid2::num_centres              = 1

CarpetRegrid2::num_levels_1             = 6
CarpetRegrid2::radius_1[1]              =  8.0
CarpetRegrid2::radius_1[2]              =  4.0
CarpetRegrid2::radius_1[3]              =  2.0
CarpetRegrid2::radius_1[4]              =  1.0
CarpetRegrid2::radius_1[5]              =  0.5


Carpet::use_buffer_zones                = yes
Carpet::prolongation_order_space        = 5
Carpet::prolongation_order_time         = 2

CarpetRegrid2::freeze_unaligned_levels  = yes
CarpetRegrid2::regrid_every             = -1

CarpetRegrid2::verbose                  = no

Carpet::grid_structure_filename         = "carpet-grid-structure"
Carpet::grid_coordinates_filename       = "carpet-grid-coordinates"

Carpet::time_refinement_factors         = "[1, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512]"
Time::dtfac                             = 0.25


# Initial Data
#------------------------------------------------------------------------------

ADMBase::initial_data                 = "twopunctures"
ADMBase::initial_lapse                = "one"
ADMBase::initial_shift                = "zero"
ADMBase::initial_dtlapse              = "zero"
ADMBase::initial_dtshift              = "zero"

ADMBase::lapse_timelevels             = 3
ADMBase::shift_timelevels             = 3
ADMBase::metric_timelevels            = 3

TwoPunctures::swap_xz            = "yes"
TwoPunctures::par_b              =  1.   # trick for single BHs
TwoPunctures::center_offset[2]   = -1.   # trick for single BHs. this
                                                   # must be consistent with swap_xz parameter
TwoPunctures::give_bare_mass     = no
TwoPunctures::target_M_plus      = 1.0
TwoPunctures::target_M_minus     = 0.0
TwoPunctures::par_m_plus         = 1.0
TwoPunctures::par_m_minus        = 0.0

TwoPunctures::par_P_plus[0]      = 0.0
TwoPunctures::par_S_plus[0]      = 0.0  # must be consistent with swap_xz parameter
#TwoPunctures::par_S_plus[0]      = -0.975   # must be consistent with swap_xz parameter

TwoPunctures::par_P_minus[0]     = 0.0
TwoPunctures::par_S_minus[0]     = 0.0

TwoPunctures::TP_epsilon         = 1.0d-6
TwoPunctures::TP_Tiny            = 1.0d-10

TwoPunctures::npoints_A          = 24
TwoPunctures::npoints_B          = 24
TwoPunctures::npoints_phi        = 8
TwoPunctures::grid_setup_method  = "evaluation"
##TwoPunctures::Newton_maxit       = 12
##TwoPunctures::Newton_tol         = 1.0e-10


TwoPunctures::keep_u_around      = yes
TwoPunctures::verbose            = yes

InitBase::initial_data_setup_method   = "init_some_levels"
Carpet::init_fill_timelevels          = yes
Carpet::init_3_timelevels             = no


# Evolution
#------------------------------------------------------------------------------

ADMBase::evolution_method         = "LeanBSSNMoL"
ADMBase::lapse_evolution_method   = "LeanBSSNMoL"
ADMBase::shift_evolution_method   = "LeanBSSNMoL"
ADMBase::dtlapse_evolution_method = "LeanBSSNMoL"
ADMBase::dtshift_evolution_method = "LeanBSSNMoL"

LeanBSSNMoL::impose_conf_fac_floor_at_initial   = yes
LeanBSSNMoL::conf_fac_floor                     = 1.0d-04
LeanBSSNMoL::precollapsed_lapse                 = yes
LeanBSSNMoL::eta_beta                     = 1
LeanBSSNMoL::eta_beta_dynamic             = no
LeanBSSNMoL::derivs_order                 = 4
LeanBSSNMoL::use_advection_stencils       = yes
LeanBSSNMoL::calculate_constraints        = yes


# Spatial finite differencing
#------------------------------------------------------------------------------

Dissipation::epsdis = 0.15
Dissipation::order  = 5
Dissipation::vars   = "
  ADMBase::lapse
  ADMBase::shift
  LeanBSSNMoL::conf_fac
  LeanBSSNMoL::hmetric
  LeanBSSNMoL::hcurv
  LeanBSSNMoL::trk
  LeanBSSNMoL::gammat
"


# Integration method
#------------------------------------------------------------------------------

MoL::ODE_Method                 = "RK4"
MoL::MoL_Intermediate_Steps     = 4
MoL::MoL_Num_Scratch_Levels     = 1

Carpet::num_integrator_substeps = 4


# Spherical surfaces
#------------------------------------------------------------------------------
# Use surface[0] for apparent horizon
# Use surface[1] for puncture tracker

SphericalSurface::nsurfaces = 2
SphericalSurface::maxntheta = 66
SphericalSurface::maxnphi   = 124
SphericalSurface::verbose   = no

# Horizon 1
SphericalSurface::ntheta            [0] = 41
SphericalSurface::nphi              [0] = 80
SphericalSurface::nghoststheta      [0] = 2
SphericalSurface::nghostsphi        [0] = 2
CarpetMask::excluded_surface        [0] = 0
CarpetMask::excluded_surface_factor [0] = 1.0

CarpetMask::verbose = no

# Puncture tracking
#------------------------------------------------------------------------------
# Use surface[0] for apparent horizon
# Use surface[1] for puncture tracker
CarpetTracker::surface[1]                       = 0
#
PunctureTracker::track                      [0] = yes
PunctureTracker::initial_x                  [0] = 0
PunctureTracker::which_surface_to_store_info[0] = 1
PunctureTracker::verbose                        = no


# Wave extraction
#------------------------------------------------------------------------------

NPScalars::NP_order     = 4
#
Multipole::out_every    = 1
Multipole::l_max        = 4
Multipole::output_hdf5  = no
Multipole::output_ascii = yes
Multipole::integration_method = "Simpson"

Multipole::nradii       = 1
Multipole::radius[0]    = 30
Multipole::variables    = "
  NPScalars::psi4re{sw=-2 cmplx='NPScalars::psi4im' name='NP_Psi4'}
"

# Horizons
#------------------------------------------------------------------------------

# AHFinderDirect::verbose_level                           = "algorithm highlights"
AHFinderDirect::verbose_level                            = "physics details"
AHFinderDirect::output_BH_diagnostics                    = "true"
AHFinderDirect::run_at_CCTK_POST_RECOVER_VARIABLES       = no

AHFinderDirect::N_horizons                               = 1
AHFinderDirect::find_every                               = 4

AHFinderDirect::output_h_every                           = 0
AHFinderDirect::max_Newton_iterations__initial           = 50
AHFinderDirect::max_Newton_iterations__subsequent        = 50
AHFinderDirect::max_allowable_Theta_growth_iterations    = 10
AHFinderDirect::max_allowable_Theta_nonshrink_iterations = 10
AHFinderDirect::geometry_interpolator_name               = "Lagrange polynomial interpolation"
AHFinderDirect::geometry_interpolator_pars               = "order=4"
AHFinderDirect::surface_interpolator_name                = "Lagrange polynomial interpolation"
AHFinderDirect::surface_interpolator_pars                = "order=4"

AHFinderDirect::move_origins                             = no

AHFinderDirect::origin_x                             [1] = 0
AHFinderDirect::initial_guess__coord_sphere__x_center[1] = 0
AHFinderDirect::initial_guess__coord_sphere__radius  [1] = 0.25
AHFinderDirect::which_surface_to_store_info          [1] = 0
AHFinderDirect::set_mask_for_individual_horizon      [1] = no
AHFinderDirect::reset_horizon_after_not_finding      [1] = no
AHFinderDirect::max_allowable_horizon_radius         [1] = 3


# Isolated Horizons
#-------------------------------------------------------------------------------

QuasiLocalMeasures::verbose                = yes
QuasiLocalMeasures::veryverbose            = no
QuasiLocalMeasures::interpolator           = "Lagrange polynomial interpolation"
QuasiLocalMeasures::interpolator_options   = "order=4"
QuasiLocalMeasures::spatial_order          = 4
QuasiLocalMeasures::num_surfaces           = 1
QuasiLocalMeasures::surface_index      [0] = 0


# Check for NaNs
#-------------------------------------------------------------------------------

Carpet::poison_new_timelevels = yes
CarpetLib::poison_new_memory  = yes
Carpet::check_for_poison      = no

NaNChecker::check_every     = 256
NanChecker::check_after     = 0
NaNChecker::report_max      = 10
# NaNChecker::verbose         = "all"
NaNChecker::action_if_found = "terminate"
NaNChecker::out_NaNmask     = yes
NaNChecker::check_vars      = "
  LeanBSSNMoL::conf_fac
"


# Timers
#-------------------------------------------------------------------------------

Cactus::cctk_timer_output               = "full"
TimerReport::out_every                  = 5120
TimerReport::n_top_timers               = 40
TimerReport::output_all_timers_together = yes
TimerReport::output_all_timers_readable = yes
TimerReport::output_schedule_timers     = no


# I/O thorns
#-------------------------------------------------------------------------------
IO::out_dir                             = "LeanCart_SBH_Diss15_V2/Output"
IOScalar::one_file_per_group            = yes
IOASCII::one_file_per_group             = yes

IOBasic::outInfo_every                  = 10
IOBasic::outInfo_reductions             = "minimum maximum"
IOBasic::outInfo_vars                   = "
Carpet::physical_time_per_hour
SystemStatistics::maxrss_mb
SystemStatistics::swap_used_mb
"

IOScalar::outScalar_every               = 0
IOScalar::outScalar_reductions          = "minimum maximum average"
IOScalar::outScalar_vars                = "SystemStatistics::process_memory_mb"

IOASCII::out0D_every                    = 10
IOASCII::out0D_vars                     = "
Carpet::timing
PunctureTracker::pt_loc
QuasiLocalMeasures::qlm_scalars{out_every = 64}
"


IOASCII::out1D_every                    = 256
IOASCII::out1D_d                        = no
IOASCII::out1D_x                        = yes
IOASCII::out1D_y                        = no
IOASCII::out1D_z                        = yes
IOASCII::out1D_vars                     = "
ADMBase::lapse
ADMBase::metric
ADMBase::curv
LeanBSSNMoL::ham
LeanBSSNMoL::mom
"




# Checkpointing and recovery
#-------------------------------------------------------------------------------
CarpetIOHDF5::checkpoint                    = yes
IO::checkpoint_ID                           = no
IO::recover                                 = "autoprobe"
IO::out_proc_every                          = 2
IO::checkpoint_on_terminate                 = yes
IO::checkpoint_every_walltime_hours         = 8
IO::checkpoint_dir                          = "LeanCart_SBH_Diss15_V2/Checkpoints"
IO::recover_dir                             = "LeanCart_SBH_Diss15_V2/Checkpoints"
IO::abort_on_io_errors                      = yes
CarpetIOHDF5::open_one_input_file_at_a_time = yes
CarpetIOHDF5::compression_level             = 0


# Run termination
#-------------------------------------------------------------------------------
TerminationTrigger::max_walltime = 48
#Trigger termination 30 minutes before the walltime is reached
TerminationTrigger::on_remaining_walltime        = 30
TerminationTrigger::output_remtime_every_minutes = 30
TerminationTrigger::termination_from_file        = yes
TerminationTrigger::termination_file             = "terminate.txt"
TerminationTrigger::create_termination_file      = yes

Cactus::terminate                               = time
Cactus::cctk_final_time                         = 100
