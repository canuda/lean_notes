%\documentclass[twocolumn,showpacs,preprintnumbers,nofootinbib,prd,superscriptaddress,groupedaddress,10pt]{revtex4-1}
\documentclass[twocolumn,floats,floatfix,showpacs,prd,superscriptaddress,nofootinbib]{revtex4-1}

%=== load packages ===
\usepackage{graphicx,epsfig}
\usepackage{amssymb,amsmath,amsthm,amsfonts}
\usepackage{bm} 

\usepackage[linktocpage]{hyperref}
\usepackage[caption=false]{subfig}
\usepackage[usenames]{color}
%\usepackage{hyperref}
\usepackage{url}

%=== define new commands ===
\newcommand{\US}[1]{\textcolor{green}{[{\it\textbf{US: #1}}]} }
\newcommand{\HW}[1]{\textcolor{red}{[{\it\textbf{HW: #1}}]} }
\newcommand{\MZ}[1]{\textcolor{blue}{[{\it\textbf{MZ: #1}}]} }
%
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\newcommand{\Ord}[2]{\mathcal O \left(#1\right)^{#2}}
\newcommand{\Tr}{\mbox{Tr}}
\newcommand{\dps}{\displaystyle}        

%=== define abbreviations ===
\def\non{\nonumber}
\def\dif{\textrm{d}}
\def\p{\partial}
\def\na{\nabla}
\def\Lap{\triangle}
\def\half{{\textstyle{1\over2}}}
%
\def\Lie{\mathcal{L}}
\def\G{\mathcal{G}}
\def\H{\mathcal{H}}
\def\M{\mathcal{M}}
\def\R{\mathcal{R}}
%

\begin{document}

\title{Notes: Convergence of LeanMoL}

\author{Helvi Witek}\email{Helvi.Witek@nottingham.ac.uk}
\affiliation{School of Mathematical Sciences, University of Nottingham,
University Park, Nottingham, NG7 2RD, UK}

\author{Miguel Zilh\~{a}o}\email{mzilhao@ffn.ub.es}
\affiliation{Departament de F\'{\i}sica Fonamental \& Institut de Ci\`{e}ncies del Cosmos, Universitat de Barcelona, Mart\'{\i} i Franqu\`{e}s 1, E-08028 Barcelona, Spain}

\author{Ulrich Sperhake}\email{us248@maths.cam.ac.uk}
\affiliation{Department of Applied Mathematics and Theoretical Physics,
Centre for Mathematical Sciences, University of Cambridge,
Wilberforce Road, Cambridge CB3 0WA, UK}



%\date{\today}
\begin{abstract}
%
XXXX
%
\end{abstract}

%\pacs{}
%04.25.D-    Numerical relativity
%04.25.dc    Numerical studies of critical behavior, singularities, and cosmic censorship
%04.25.dg    Numerical studies of black holes and black-hole binaries
%04.70.-s    Physics of black holes
%04.70.Bw    Classical black holes

\maketitle
%
\tableofcontents

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In order to test the convergence of the upgraded {\textsc{Lean}} code
we have simulated an inspiraling black-hole binary
with initial positions $z_{\pm} / M = 3$ and
bare mass parameters $m_{\pm} = 0.4765$ (resulting in $M_{\pm} = 0.5$).
%
We have evolved the binary with the {\textsc{LeanBSSNMoL}} evolution thorn,
which employs the BSSN formulation of Einstein's equations combined with moving puncture gauge.
%
We add (fifth-order) Kreiss-Oliger dissipation using 
either the {\textsc{Dissipation}} thorn provided
by the {\textsc{Einstein Toolkit}} or our own implementation.
%
The former allows only to add dissipation after each Runge-Kutta step,
whereas the latter enables us to add it after the completion of the Runge-Kutta iteration.
%
By default we add artificial dissipation on all refinement levels.

We extract gravitational radiation using the ET thorn {\textsc{WeylScal4}} and the {\textsc{Lean}} thorn
{\textsc{NPScalars}} to compute the gridfunctions of the Newman-Penrose scalar $\Psi_{4}$.
It's mode decomposition is performed with ET's {\textsc{Multipole}} thorn.

%
The grid has been set up using bitant symmetry, with ``radius'' $x^{i}_{\rm{max}}/M=120$
and using resolutions $dx_{\rm{c}} = 1.25$, $dx_{\rm{m}} = 1.0$ and $dx_{\rm{h}} = 0.75$ on the outermost of the seven refinement levels.


We summarize the set of simulations in Table~\ref{tab:RunsLeanMoL}.
We denote the evolution thorn ({\textsc{LeanBSSNMoL}})
\HW{add evolutions that Miguel did with {\textsc{simpleRK6chiBSSN}} }
the employed dissipation thorn (``dissET'' or ``dissLeanPostRK'') and 
dissipation parameter $\sigma_{\rm{diss}}$. 


%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------
\begin{table*}[tbhp]
\centering
\caption{\label{tab:RunsLeanMoL}
List with runs to test the convergence of LeanMoL.
We denote the evolution thorn ({\textsc{LeanBSSNMoL}}, \HW{add {\textsc{simpleRK6chiBSSN}} })
the employed dissipation thorn (``dissET'' or ``dissLeanPostRK'') and 
dissipation parameter $\sigma_{\rm{diss}}$. 
}
\input{Tables/Table_RunsLeanMoL.tex}
\end{table*}
%-----------------------------------------------------------------------------


%--- Convergence plots LeanMoL -----------------------------------------------
\begin{figure*}[htpb!]
\subfloat[Waveforms]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_NoDiss/Plot_Convergence_NP_Psi4_l2m2_NoDiss}\label{fig:LeanMoL_NoDissWaveforms}}
\subfloat[Hamiltonian]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_NoDiss/Plot_Convergence_Ham_t100_NoDiss}\label{fig:LeanMoL_NoDissHamiltonian100}}
\caption{\label{fig:LeanMoL_NoDiss}
Convergence test for run {\texttt{LeanMoL\_NoDiss}}.
%\protect\verb|LeanMoL\_NoDiss|
In panel~\protect\subref{fig:LeanMoL_NoDissWaveforms} we present the difference in waveforms obtained from the 
coarse and medium, and medium and high resolution runs. The differences are {\textit{not}} rescaled and,
since the convergence factor are all larger than one, do {\textit{not}} show convergence.
The third curve shows the difference between the medium and high resolution runs, {\textit{experimentally}} rescaled to match
the difference between the coarse and medium runs.
%
In panel~\protect\subref{fig:LeanMoL_NoDissHamiltonian100} we show the Hamiltonian constraint along the x-axis 
after an evolution time of $t \sim 100$ which corresponds approximately to the time of the merger. The factors
$Q_{3c} = (dx_{\rm{c}}/dx_{\rm{h}})^{3} = 0.21$ and $Q_{3m} = (dx_{\rm{m}}/dx_{\rm{h}})^{3} = 0.42$
indicate third order convergence. We obtain a similar behaviour at around $t=200$, well after the merger. 
}
\end{figure*}
%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------
\begin{figure*}[htpb!]
\subfloat[Waveforms]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_LeanPRK/Plot_Convergence_NP_Psi4_l2m2_LeanPRK}\label{fig:LeanMoL_PRKWaveforms}}
\subfloat[Hamiltonian]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_LeanPRK/Plot_Convergence_Ham_t100_LeanPRK}\label{fig:LeanMoL_PRKHamiltonian100}}
\caption{\label{fig:LeanMoL_PRK}
Convergence test for run {\texttt{LeanMoL\_PRK}}.
%\protect\verb|LeanMoL\_NoDiss|
In panel~\protect\subref{fig:LeanMoL_PRKWaveforms}
we present the difference in waveforms obtained from the 
coarse and medium, and medium and high resolution runs. The differences are rescaled by $Q_{4}=2.11$ that would be expected
and we do {\textit{not}} see convergence (or ``over-convergence''?).
%
In panel~\protect\subref{fig:LeanMoL_NoDissHamiltonian100} we show the Hamiltonian constraint along the x-axis 
after an evolution time of $t \sim 100$ which corresponds approximately to the time of the merger. The factors
$Q_{2c} = (dx_{\rm{c}}/dx_{\rm{h}})^{2} = 0.36$ and $Q_{2m} = (dx_{\rm{m}}/dx_{\rm{h}})^{2} = 0.56$
indicate approximately second order convergence.
We obtain a similar behaviour at around $t=200$, well after the merger. 
}
\end{figure*}
%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------
\begin{figure*}[htpb!]
\subfloat[Waveforms]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ETD_s01/Plot_Convergence_NP_Psi4_l2m2_ETD_s01}\label{fig:LeanMoL_ETD_s01Waveforms}}
\subfloat[Hamiltonian]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ETD_s01/Plot_Convergence_Ham_t100_ETD_s01}\label{fig:LeanMoL_ETD_s01Hamiltonian100}}
\caption{\label{fig:LeanMoL_ETD_s01}
Convergence test for run {\texttt{LeanMoL\_ETD\_s01}}
%\protect\verb|LeanMoL\_NoDiss|
In panel~\protect\subref{fig:LeanMoL_ETD_s01Waveforms} we present the difference in waveforms obtained from the 
coarse and medium, and medium and high resolution runs. The differences are {\textit{not}} rescaled and,
since the convergence factor are all larger than one, do {\textit{not}} show convergence.
The third curve shows the difference between the medium and high resolution runs, {\textit{experimentally}} rescaled to match
the difference between the coarse and medium runs.
%
In panel~\protect\subref{fig:LeanMoL_ETD_s01Hamiltonian100} we show the Hamiltonian constraint along the x-axis 
after an evolution time of $t \sim 100$ which corresponds approximately to the time of the merger. The factors
$Q_{3c} = (dx_{\rm{c}}/dx_{\rm{h}})^{3} = 0.21$ and $Q_{3m} = (dx_{\rm{m}}/dx_{\rm{h}})^{3} = 0.42$
indicate third order convergence. We obtain a similar behaviour at around $t=200$, well after the merger. 
}
\end{figure*}
%%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------
\begin{figure*}[htpb!]
\subfloat[Waveforms]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ETD_s02/Plot_Convergence_NP_Psi4_l2m2_ETD_s02}\label{fig:LeanMoL_ETD_s02Waveforms}}
\subfloat[Hamiltonian]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ETD_s02/Plot_Convergence_Ham_t100_ETD_s02}\label{fig:LeanMoL_ETD_s02Hamiltonian100}}
\caption{\label{fig:LeanMoL_ETD_s02}
Convergence test for run {\texttt{LeanMoL\_ETD\_s02}}
%\protect\verb|LeanMoL\_NoDiss|
In panel~\protect\subref{fig:LeanMoL_ETD_s02Waveforms} we present the difference in waveforms obtained from the 
coarse and medium, and medium and high resolution runs. The differences are {\textit{not}} rescaled and,
since the convergence factor are all larger than one, do {\textit{not}} show convergence.
The third curve shows the difference between the medium and high resolution runs, {\textit{experimentally}} rescaled to match
the difference between the coarse and medium runs.
%
In panel~\protect\subref{fig:LeanMoL_ETD_s02Hamiltonian100} we show the Hamiltonian constraint along the x-axis 
after an evolution time of $t \sim 100$ which corresponds approximately to the time of the merger. The factors
$Q_{4c} = (dx_{\rm{c}}/dx_{\rm{h}})^{3} = 0.13$ and $Q_{4m} = (dx_{\rm{m}}/dx_{\rm{h}})^{3} = 0.32$
indicate fourth order convergence. We obtain a similar behaviour at around $t=200$, well after the merger. 
}
\end{figure*}
%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------
\begin{figure*}[htpb!]
\subfloat[Waveforms]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ETD_s03/Plot_Convergence_NP_Psi4_l2m2_ETD_s03}\label{fig:LeanMoL_ETD_s03Waveforms}}
\subfloat[Hamiltonian]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ETD_s03/Plot_Convergence_Ham_t100_ETD_s03}\label{fig:LeanMoL_ETD_s03Hamiltonian100}}
\caption{\label{fig:LeanMoL_ETD_s03}
Convergence test for run {\texttt{LeanMoL\_ETD\_s03}}
%\protect\verb|LeanMoL\_NoDiss|
In panel~\protect\subref{fig:LeanMoL_ETD_s03Waveforms} we present the difference in waveforms obtained from the 
coarse and medium, and medium and high resolution runs. The latter difference has been rescaled by
$Q_{2} = 1.28$ and $Q_{3} = 1.64$, indicating between second and third order convergence. 
%
In panel~\protect\subref{fig:LeanMoL_ETD_s03Hamiltonian100} we show the Hamiltonian constraint along the x-axis 
after an evolution time of $t \sim 100$ which corresponds approximately to the time of the merger. The factors
$Q_{4c} = (dx_{\rm{c}}/dx_{\rm{h}})^{4} = 0.13$ and $Q_{4m} = (dx_{\rm{m}}/dx_{\rm{h}})^{3} = 0.32$
indicate fourth order convergence. We obtain a similar behaviour at around $t=200$, well after the merger. 
}
\end{figure*}
%-----------------------------------------------------------------------------
\begin{figure*}[htpb!]
\subfloat[Waveforms]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ETD_s04/Plot_Convergence_NP_Psi4_l2m2_ETD_s04}\label{fig:LeanMoL_ETD_s04Waveforms}}
\subfloat[Hamiltonian]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ETD_s04/Plot_Convergence_Ham_t100_ETD_s04}\label{fig:LeanMoL_ETD_s04Hamiltonian100}}
\caption{\label{fig:LeanMoL_ETD_s04}
Convergence test for run {\texttt{LeanMoL\_ETD\_s04}}
%\protect\verb|LeanMoL\_NoDiss|
In panel~\protect\subref{fig:LeanMoL_ETD_s04Waveforms} we present the difference in waveforms obtained from the 
coarse and medium, and medium and high resolution runs. The latter difference has been rescaled by
$Q_{4} = 2.11$ indicating fourth order convergence.
%
In panel~\protect\subref{fig:LeanMoL_ETD_s04Hamiltonian100} we show the Hamiltonian constraint along the x-axis 
after an evolution time of $t \sim 100$ which corresponds approximately to the time of the merger. The factors
$Q_{4c} = (dx_{\rm{c}}/dx_{\rm{h}})^{4} = 0.13$ and $Q_{4m} = (dx_{\rm{m}}/dx_{\rm{h}})^{4} = 0.32$
indicate fourth order convergence. We obtain a similar behaviour at around $t=200$, well after the merger. 
}
\end{figure*}
%-----------------------------------------------------------------------------
\begin{figure*}[htpb!]
\subfloat[Waveforms]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ETD_s05/Plot_Convergence_NP_Psi4_l2m2_ETD_s05}\label{fig:LeanMoL_ETD_s05Waveforms}}
\subfloat[Hamiltonian]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ETD_s05/Plot_Convergence_Ham_t100_ETD_s05}\label{fig:LeanMoL_ETD_s05Hamiltonian100}}
\caption{\label{fig:LeanMoL_ETD_s05}
Convergence test for run {\texttt{LeanMoL\_ETD\_s05}}
%\protect\verb|LeanMoL\_NoDiss|
In panel~\protect\subref{fig:LeanMoL_ETD_s05Waveforms} we present the difference in waveforms obtained from the 
coarse and medium, and medium and high resolution runs. The latter difference has been rescaled by
$Q_{6} = 3.42$ indicating sixth order (i.e. over-) convergence.
%
In panel~\protect\subref{fig:LeanMoL_ETD_s05Hamiltonian100} we show the Hamiltonian constraint along the x-axis 
after an evolution time of $t \sim 100$ which corresponds approximately to the time of the merger. The factors
$Q_{4c} = (dx_{\rm{c}}/dx_{\rm{h}})^{4} = 0.13$ and $Q_{4m} = (dx_{\rm{m}}/dx_{\rm{h}})^{4} = 0.32$
indicate fourth order convergence. We obtain a similar behaviour at around $t=200$, well after the merger. 
}
\end{figure*}
%-----------------------------------------------------------------------------

%-----------------------------------------------------------------------------
\begin{figure*}[htpb!]
\subfloat[$\sigma_{\rm{Diss}}=0.1$]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ML/Plot_Convergence_ML_Psi4_l2m2_s01}\label{fig:ML_s01Waveforms}}
\subfloat[$\sigma_{\rm{Diss}}=0.4$]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ML/Plot_Convergence_ML_Psi4_l2m2_s04}\label{fig:ML_s04Waveforms}}
\caption{\label{fig:ML_Waveforms}
Convergence test for evolutions with McLachlan
% {\texttt{LeanMoL\_ETD\_s05}}
In panel~\protect\subref{fig:ML_s01Waveforms} we present the 
convergence tests for run {\texttt{ML\_ETD\_s01}} using the {\textsc{McLachlan}} evolution thorn 
and a dissipation parameter $\sigma_{\rm{Diss}}=0.1$.
The rescaling factor $Q_{2}=1.28$ indicates second order convergence.
%
In panel~\protect\subref{fig:ML_s04Waveforms} we present the 
convergence tests for run {\texttt{ML\_ETD\_s04}} using the {\textsc{McLachlan}} evolution thorn 
and a dissipation parameter $\sigma_{\rm{Diss}}=0.4$.
The rescaling factor $Q_{2}=1.28$ indicates second order convergence.
}
\end{figure*}
%-----------------------------------------------------------------------------

%-----------------------------------------------------------------------------
\begin{figure*}[htpb!]
\subfloat[$\sigma_{\rm{Diss}}=0.2$]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_SimpleBSSN_s02/Plot_Convergence_NP_Psi4_l2m2_simpleBSSN_s02}\label{fig:SimpleBSSN_WaveformsS02}}
%\subfloat[$\sigma_{\rm{Diss}}=0.4$]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ML/Plot_Convergence_ML_Psi4_l2m2_s04}\label{fig:ML_s04Waveforms}}
\caption{\label{fig:SimpleBSSN_Waveforms}
Convergence test for evolutions with (``intermediate'') Lean
% {\texttt{LeanMoL\_ETD\_s05}}
In panel~\protect\subref{fig:SimpleBSSN_WaveformsS02} we present the 
convergence tests for run {\texttt{LeanSimple\_DRK\_s02}} using the {\textsc{simpleRK6chiBSSN}} evolution thorn 
and adding dissipation after each RK step with $\sigma_{\rm{Diss}}=0.2$.
We have not rescaled the difference between the medium and high resolution runs,
indicating no convergence.
%
}
\end{figure*}
%-----------------------------------------------------------------------------

%-----------------------------------------------------------------------------
\begin{figure*}[htpb!]
\subfloat[$\sigma_{\rm{Diss}}=0.2$]{\includegraphics[width=0.33\textwidth,clip]{Plots/Plots_OldLean_s02/Plot_Convergence_NP_Psi4_l2m2_OldLean_s02}\label{fig:oldlean_WaveformsS02}}
\subfloat[$\sigma_{\rm{Diss}}=0.4$]{\includegraphics[width=0.33\textwidth,clip]{Plots/Plots_OldLean_s04/Plot_Convergence_NP_Psi4_l2m2_OldLean_s04}\label{fig:oldlean_WaveformsS04}}
\subfloat[$\sigma_{\rm{Diss}}=0.2$, $\chi_{\rm{floor}}=10^{-4}$]{\includegraphics[width=0.33\textwidth,clip]{Plots/Plots_OldLean_s02_chi04/Plot_Convergence_NP_Psi4_l2m2_OldLean_s02_chi04}\label{fig:oldlean_WaveformsS02_chi04}}
\caption{\label{fig:oldlean_Waveforms}
Convergence test for evolutions with the old {\textsc{Lean}} implementation.
% {\texttt{LeanMoL\_ETD\_s05}}
In panel~\protect\subref{fig:oldlean_WaveformsS02} we present the 
convergence tests for run {\texttt{OldLean\_DRK\_s02}} using the old {\textsc{Lean}} implementation
and adding dissipation after each RK step with $\sigma_{\rm{Diss}}=0.2$.
The rescaling factor $Q_{4}=2.19$ indicates fourth order convergence.
%
In panel~\protect\subref{fig:oldlean_WaveformsS04} we present the 
convergence tests for run {\texttt{OldLean\_DRK\_s04}} using the old {\textsc{Lean}} implementation
and adding dissipation after each RK step with $\sigma_{\rm{Diss}}=0.4$.
The rescaling factor $Q_{4}=2.19$ indicates fourth order convergence.
%
In panel~\protect\subref{fig:oldlean_WaveformsS02_chi04}
we present the convergence tests for run {\texttt{OldLean\_DRK\_s02}} using the old {\textsc{Lean}} implementation
and adding dissipation after each RK step with $\sigma_{\rm{Diss}}=0.2$ 
and using the floor value $\chi_{\rm{floor}}=10^{04}$ for the conformal factor.
The rescaling factor $Q_{4}=2.19$ indicates fourth order convergence.
%
Note, however, that it was necessary to use a slightly different grid setup and resolutions!
Specifically, we set $\{(128,32,16,8)(2,1,0.5),h\}$ with $h=1/52, 1/64,1/84$ corresponding to
$dx=1.23,1.0,0.8$ on the outermost refinement level.
%
Note, also, that the differences between the coarse-medium and medium-high resolution are more than an order of magnitude larger than in the test for the new implementation.
}
\end{figure*}
%-----------------------------------------------------------------------------


%-----------------------------------------------------------------------------
\begin{figure*}[htpb!]
\subfloat[$\sigma_{\rm{diss}}=0.2$, $\chi_{\rm{floor}}=10^{-4}$]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ETD_s02_chi04/Plot_Convergence_NP_Psi4_l2m2_ETD_s02_chi04}\label{fig:LeanMoL_ETD_chi04_s02}}
\subfloat[$\sigma_{\rm{diss}}=0.4$, $\chi_{\rm{floor}}=10^{-4}$]{\includegraphics[width=0.45\textwidth,clip]{Plots/Plots_ETD_s04_chi04/Plot_Convergence_NP_Psi4_l2m2_ETD_s04_chi04}\label{fig:LeanMoL_ETD_chi04_s04}}
\caption{\label{fig:LeanMoL_ETD_chi04}
Convergence test for runs {\texttt{LeanMoL\_ETD\_s02\_chi04}} and {\texttt{LeanMoL\_ETD\_s04\_chi04}}.
%\protect\verb|LeanMoL\_NoDiss|
In panel~\protect\subref{fig:LeanMoL_ETD_chi04_s02} we present the difference in waveforms obtained from the 
coarse and medium, and medium and high resolution runs. 
The latter difference has been rescaled by $Q_{3} = 1.64$, indicating third order convergence.
%
In panel~\protect\subref{fig:LeanMoL_ETD_chi04_s04}
we present the difference in waveforms obtained from the 
coarse and medium, and medium and high resolution runs. 
The latter difference has been rescaled by $Q_{3} = 1.64$, indicating third order convergence.
}
\end{figure*}
%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------


%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\clearpage
% References
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\nocite{*}
\bibliographystyle{h-physrev4}
\bibliography{Refs_LeanMoL.bib}

\end{document}

